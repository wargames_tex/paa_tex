# -*- Makefile *-
#
# Common variables
#
ifndef	LATEX
LATEX		:= pdflatex
endif

LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex 15			\
		   -shell-escape
EXPORT          := $(shell kpsewhich wgexport.py)
EXPORT_FLAGS	:= 
PDFTOCAIRO	:= pdftocairo
CAIROFLAGS	:= -scale-to 800
PDFJAM		:= pdfjam
REDIR		:= > /dev/null 2>&1 

SUBMAKE_FLAGS	:= --no-print-directory

ifdef SVG
EXPORT_FLAGS	:= -I svg
VMOD_RES	:= 210
endif
ifndef VMOD_DESC
VMOD_DESC	:= This module was made from LaTeX sources
endif
ifndef VMOD_RES
VMOD_RES	:= 150
endif
ifndef VMOD_SCALE
VMOD_SCALE	:= 1
endif

ifdef VERBOSE	
MUTE		:=
REDIR		:=
SUBMAKE_FLAGS	:=
LATEX_FLAGS	:= -synctex 15	-shell-escape
EXPORT_FLAGS	+= -V 
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif

#
# EOF
#
