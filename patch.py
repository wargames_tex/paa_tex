from wgexport import *
from pprint import pprint

# TODO:
# - A lot
# 
# - Commands for supplies (eliminate etc.)
#   - Auto maximum supply on combat declaration? Probably not
# - Counter attacks
#   - Find way to limit sub-sequent counter attacks? Probably not
# - Out-of-C2 
#   - Prevent checkC2+'Force' to be re-done? Probably not 
#   - Prevent resetC2 to be re-done? Probably not
# - Optional rules
#   - Reaction movement (no effect, use recalculate odds)
# - VP
#   - Do count OOC2 (no issue, Flipped end of allied turn)

# --------------------------------------------------------------------
def makePretty(name):
    typeEche = {'ibde':   'Infantry Brigade',
                'ibn':    'Infantry Battalion',
                'iregt':  'Infantry Regiment',
                'aibde':  'Mechanised Infantry Brigade',
                'mibde':  'Motorised Infantry Brigade',
                'airegt': 'Mechanised Infantry Regiment',
                'abiregt':'Airborne Infantry Regiment',
                'aairegt':'Airborne Infantry Regiment',
                'aibtn':  'Mechanised Infantry Battalion',
                'abibde': 'Airborne Infantry Brigade',
                'abde':   'Armoured Brigade',
                'abn':    'Armoured Battalion',
                'aregt':  'Armoured Regiment',
                'arecbn': 'Armoured Reconnaissance Battalion',
                'rp':     'Replacement points'}
    nat = { 'de': 'German',
            'it': 'Italian',
            'br': 'British',
            'in': 'Indian',
            'sa': 'South African',
            'nz': 'New Zealand',
            'au': 'Australian',
            'us': 'United States',
            'pl': 'Polish',
            'gk': 'Greek',
            'fr': 'French',
            'ff': 'French',
            'be': 'Belgian'
           }
    others = {'gu':   'Gurka',
              'bers': 'Bersagliere',
              'g':    'Guards',
              'sg':   'Support Group',
              'at':   'Army',
              'ggff': 'Giovane Fascisti',
              'ram':  "Ramcke's"
              }
    
    def ords(num):
        if num % 10 == 1 and num != 11: return 'st'
        if num % 10 == 2 and num != 12: return 'nd'
        if num % 10 == 3 and num != 13: return 'rd'
        return 'th'

    def try_ords(s):
        try:
            n = int(s)
            return str(n)+ords(n)
        except:
            pass

        return s
    
    if name == 'br locomotive':
        return 'Eqyptian railway'
    if name == 'de malta':
        return 'Malta invasion'
        
    
    parts = name.split()
    end   = parts[-1]
    # print(f'{name:30s} -> {parts}')
    last  = typeEche.get(parts.pop(),None)
    first = nat.get(parts.pop(0),None)

    if len(parts) > 0 and (parts[0] == 'lorry' or parts[0] == 'supply'):
        typ = {'lorry': 'Transport Unit',
               'supply': 'Supply Installation'}[parts[0]]
        num = try_ords(end.capitalize())
        return first+' '+num+' '+typ

    if len(parts) > 0 and end == 'rp':
        tp = {'i': 'Infantry', 'a': 'Armoured'}[parts[0]]
        return first+' '+tp+' '+last
    
    if last is None or first is None:
        print(f'Do not know how to deal with {name}')
        return name

    if len(parts) == 0:
        return first+' '+last


    
    num=parts.pop(0)
    try:
        num = int(num)
        num = f'{num}{ords(num)}'
    except:
        num = others.get(num,num)
        pass 

    if len(parts) <= 0:
        return first+f' {num} '+last
        
    oth=parts.pop(0)
    div=None
    try:
        div = int(oth)
        div = f'{div}{ords(div)}'
    except:
        pass
    if div is not None:
        return first+f' {div} Division {num} '+last
        

    othh   = others.get(oth,None)
    if not othh:
        print(f'=== {name:30s} -> Do not know {oth}')
    
    return first+f' {othh} {num} '+last
    
    
# --------------------------------------------------------------------
def mainAtStart(main,destName,*pieces):
    dest = None
    lst  = main.getAtStarts(single=False)
    if lst is None: lst = []
    for at in lst:
        if at['name'] == destName:
            dest = at
            break
    if dest is None:
        at = main.addAtStart(name            = destName,
                             location        = destName,
                             owningBoard     = main['mapName'],
                             useGridLocation = True)

    for piece in pieces:
        at.append(piece)
    
# --------------------------------------------------------------------
def hexName(hx):
    return hx[:2]+'@'+hx[2:]

# --------------------------------------------------------------------
def concatHexes(hexes):
    return ':'+':'.join(hexes) + ':'
# --------------------------------------------------------------------
def getHexes(hexes,what):
    return [hexName(k) for k,v in hexes.items() if what in v]
# --------------------------------------------------------------------
def getSwampHexes(hexes):
    return concatHexes(getHexes(hexes,'swamp'))
# --------------------------------------------------------------------
def getRoughHexes(hexes):
    return concatHexes(getHexes(hexes,'rough'))
# --------------------------------------------------------------------
def getFortHexes(hexes):
    return concatHexes(getHexes(hexes,'fortified'))

# --------------------------------------------------------------------
def getEscarpments():
    from data import escarpments
    edges = {
        'N':   ('S',  lambda c,r:(c,r+1)),
        'NE':  ('SW', lambda c,r:(c-1,r+((c+1)%2))),
        'SE':  ('NW', lambda c,r:(c-1,r-(c%2))),
        'S':   ('N',  lambda c,r:(c,r-1)),
        'SW':  ('NE', lambda c,r:(c+1,r-(c%2))),
        'NW':  ('SE', lambda c,r:(c+1,r+((c+1)%2)))}
    samecol = {
        'NW': 'SW',
        'NE': 'SE',
        'SW': 'NW',
        'SE': 'NE' }
    
    def crName(c,r):
        return f'{c:02d}@{r:02d}'
    def getEdge(w1,w2):
        return edges.get((w1,w2),edges.get((w2,w1),None))
    def getTarget(edge,c1,r1):
        if edge is None:
            return ''
        targ = edge[1](c1,r1)
        if targ is None:
            return ''
        return crName(targ[0],targ[1])

    from re import match

    crossings = list()
    
    for hex, hexedgs in escarpments.items():
        if hexedgs is None:
            continue

        hexcol = int(hex[:2].lstrip('0'))
        hexrow = int(hex[2:].lstrip('0'))
        hexnme = crName(hexcol,hexrow)
        #print(hex)
        for edge in hexedgs:
            tgt = edges.get(edge,None)
            if tgt is None:
                continue

            tgtcol, tgtrow = tgt[1](hexcol,hexrow)
            tgtEdge,tgtnme = tgt[0],crName(tgtcol,tgtrow)
            tgtraw         = f'{tgtcol:02d}{tgtrow:02d}'

            # print(f'{hex}.{edge} -> {tgtraw}.{tgtEdge} ',end='')
            crossings.append(''.join([hexnme,tgtnme]))

            if tgtraw not in escarpments:
                # print(f'Missing?')
                crossings.append(''.join([tgtnme,hexnme]))
            elif tgtEdge not in escarpments[tgtraw]:
                # print(f'Missing edge?')
                pass 
            else:
                # print('OK')
                pass 
            
            
            #print(' ',edge,tgtHex,tgtEdge)

    #crossings = list(crossings)
    crossings.sort()

    ret = ':'+':'.join(crossings)+':'
    # print(ret)
    # for c in crossings:print(c)
    return ret

# --------------------------------------------------------------------
def addTurnPrototypes(n,
                      turns,
                      main,
                      prototypesContainer,
                      firstPhase,
                      marker='game turn',
                      zoneName='turns'):
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on.
    board = main['mapName']
    turnp = []
    for t in range(1,n+1):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        f'(Phase=="{firstPhase}")}}'),
                        reportFormat = (f'{{"=== <b>Turn "+'
                                        f'Turn+" ({t})</b> ==="}}'),
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = (f'{{BasicName == "{marker}"}}'))
        pn     = f'To turn {t}'
        reg    = f'{{"Turn "+Turn+"@turns"}}'
        traits = [
            SendtoTrait(mapName     = board,
                        boardName   = board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = zoneName,
                        destination = 'R', #R for region
                        region      = reg,
                        key         = k,
                        x           = 0,
                        y           = 0),
            #ReportTrait(k,
            #            report = f'~ Moving to turn {t}'),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = f'{pn} prototype',
                                         description = f'{pn} prototype',
                                         traits      = traits)
        turnp.append(pn)

    return turnp

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    # from os import getcwd
    from sys import path
    path.append('.')
    from pathlib import Path
    # print(getcwd())
    
    from re  import sub
    from PIL import Image
    from io  import BytesIO
    #from old import edges
    #from old import hexes
    # from data import edges
    from data import hexes


    imgs = [
        '.imgs/combat_1_after.png',
        '.imgs/combat_1_counter.png',
        '.imgs/combat_1_cresolve.png',
        '.imgs/combat_1_declare.png',
        '.imgs/combat_1_desel.png',
        '.imgs/combat_1_resolve.png',
        '.imgs/combat_2_after.png',
        '.imgs/combat_2_capture.png',
        '.imgs/combat_2_counter.png',
        '.imgs/combat_2_cresolve.png',
        '.imgs/combat_2_declare.png',
        '.imgs/combat_2_desel.png',
        '.imgs/combat_2_resolve.png',
        '.imgs/combat_2_select.png',
    ]
    for img in imgs:
        src = Path(img)
        dst = Path('images') / src.name
        vmod.addExternalFile(img,str(dst))

    game = build.getGame()


    symDices = game.getSymbolicDices()
    symDices['1d6Dice']['icon'] = 'dice-icon.png'
    
    # ================================================================
    #
    # Global properties
    #
    # ----------------------------------------------------------------
    gp                = game.getGlobalProperties()[0];
    # ----------------------------------------------------------------
    #
    # Lists of hexes and crossings
    #
    fortHexes         = getFortHexes     (hexes)
    swampHexes        = getSwampHexes    (hexes)
    roughHexes        = getRoughHexes    (hexes)
    escarpments       = getEscarpments()
    globalFort        = 'Fortified'
    globalSwamp       = 'Swamp'
    globalRough       = 'Rough'
    globalEscarpments = 'Escarpments'
    
    for h,n in [[fortHexes,         globalFort       ],   
                [swampHexes,        globalSwamp      ],   
                [roughHexes,        globalRough      ],   
                [escarpments,       globalEscarpments]]:
        gp.addProperty(name         = n,
                       initialValue = h,
                       description  = f'List of {n} hexes')
    

    # ----------------------------------------------------------------
    globalDeclare   = 'NotDeclare'
    globalResolve   = 'NotResolve'
    defenderHex     = 'DefenderHex'
    counterAttack   = 'CounterAttack'
    fromCounter     = 'FromCounterAttack'
    overEscarpment  = 'OverEscarpment'
    c2Hexes         = 'C2Hexes'
    c2LastDigits    = 'C2LastDigits'
    globalSetup     = 'NotSetup'
    globalVP        = 'VP'
    globalMaxInf    = 'AlliedMaxInfantry'
    globalMaxArm    = 'AlliedMaxArmoured'
    gp              = game.getGlobalProperties()[0];
    alliedInfRP     = 'AlliedInfRP'
    alliedArmRP     = 'AlliedArmRP'
    germanInfRP     = 'GermanInfRP'
    germanArmRP     = 'GermanArmRP'
    italianInfRP    = 'ItalianInfRP'
    italianArmRP    = 'ItalianArmRP'
    maltaStatus     = 'MaltaStatus'
    maltaNotPossible= 'NoMaltaInvasion'
    supplyHex       = 'SupplyHex'
    supplyTaken     = 'SupplyTaken'
    vpPenalty       = 'VPPenalty'
    victoryPretty   = 'VictoryPretty'
    bareDF          = 'BareDF'
    itInfantryLoss  = 'ItalianInfantryLoss'
    itInfantryOpt   = 'ItalianInfantryOpt'
    reactionMove    = 'ReactionMove'
    # for no in range(1,7):
    #     gp.addProperty(name         = f'{defenderRough}{no}',
    #                    initialValue = '',
    #                    description  = f'Defender rough {no}')
    # for no in range(1,7):
    #     gp.addProperty(name         = f'{defenderSwamp}{no}',
    #                    initialValue = '',
    #                    description  = f'Defender swamp {no}')
    # for no in range(1,7):
    #     gp.addProperty(name         = f'{defenderFort}{no}',
    #                    initialValue = '',
    #                    description  = f'Defender fort {no}')
    for no in range(1,7):
        gp.addProperty(name         = f'{defenderHex}{no}',
                       initialValue = '',
                       description  = f'Defender hex {no}')
    gp.addProperty(name         = globalDeclare,
                   initialValue = 'true',
                   description  = 'True when not in declare phase')
    gp.addProperty(name         = globalResolve,
                   initialValue = 'true',
                   description  = 'True when not in combat phase')
    gp.addProperty(name         = globalSetup,
                   initialValue = 'false',
                   description  = 'True when not in setup phase')
    gp.addProperty(name         = globalMaxInf,
                   initialValue = 2, # 2,
                   description  = 'Maximum Allied infantry CF')
    gp.addProperty(name         = globalMaxArm,
                   initialValue = 3, # 3,
                   description  = 'Maximum Allied armoured CF')
    gp.addProperty(name         = supplyHex,
                   initialValue = '', # 3,
                   description  = 'Hex of last supply casualty')
    gp.addProperty(name         = supplyTaken,
                   initialValue = 0, # 3,
                   description  = 'Supply was taken')
    gp.addProperty(name         = maltaStatus,
                   #0:no attempt,1:success,2:failure
                   initialValue = 0,
                   description  = 'Status of Malta invasion')
    gp.addProperty(name         = maltaNotPossible,
                   initialValue = 'true',
                   description  = 'True Malta invasion not possible')
    gp.addProperty(name         = counterAttack,
                   initialValue = 'false',
                   description  = 'True when defener counter attacking')
    gp.addProperty(name         = fromCounter,
                   initialValue = 'false',
                   description  = 'True when defender counter attacking')
    gp.addProperty(name         = bareDF,
                   initialValue = 0,
                   isNumeric    = True,
                   description  = 'Battle DF without modifiers')
    gp.addProperty(name         = c2Hexes,
                   initialValue = '',
                   isNumeric    = False,
                   description  = 'Hexes checked for C2')
    gp.addProperty(name         = c2LastDigits,
                   initialValue = '',
                   isNumeric    = False,
                   description  = 'Last digits of C2 hexes, SPI')
    gp.addProperty(name         = itInfantryLoss,
                   initialValue = 0,
                   isNumeric    = True,
                   description  = 'Italian Infantry CF losses')
    for faction in ['Allied','Axis']:
        gp.addProperty(name         = globalVP+faction,
                       initialValue = 0,
                       description  = f'{faction} VPs')
    for faction in [alliedInfRP,alliedArmRP,
                    germanInfRP,germanArmRP,
                    italianInfRP,italianArmRP]:
        gp.addProperty(name         = faction,
                       initialValue = 0,
                       description  = f'{faction} (Replacement points)')

    crt = { 0:  [ 2, 0, 0, 0, 0, 0],
            1:  [ 2, 0, 0, 0, 0, 0],
            2:  [ 3, 3, 2, 2, 0, 0],
            3:  [ 4, 3, 3, 2, 2, 2],
            4:  [ 4, 4, 3, 3, 3, 3],
            5:  [ 6, 4, 4, 3, 3, 3],
            6:  [ 6, 6, 4, 4, 4, 4],
            7:  [ 6, 6, 6, 6, 6, 4],
            8:  [ 8, 8, 6, 6, 6, 6],
            9:  [10, 8, 8, 8, 6, 6],
            10: [10,10, 8, 8, 8, 6] }
    withdraw_slots = {
        2.1:  'br_i_withdraw_Allied_OOB',
        2.2:  'br_i_withdraw_Allied_OOB_1',
        5.1:  'br_i_withdraw_cadre_Allied_OOB',
        6.1:  'br_i_withdraw_Allied_OOB_2',
        6.2:  'br_i_withdraw_cadre_Allied_OOB_1',
        9.1:  'br_a_withdraw_Allied_OOB',
        10.1: 'br_a_withdraw_Allied_OOB_1',
        14.1: 'in_i_withdraw_Allied_OOB',
        14.2: 'in_i_withdraw_Allied_OOB_1',
        19.1: 'br_a_withdraw_Allied_OOB_2'}
    reenter_slots = {
        5.1:  'br i reenter 5 1',
        5.2:  'br i reenter 5 2',
        15.1: 'br i reenter 15 1',
        15.2: 'br i reenter 15 2',
        15.3: 'br i reenter 15 3',
    }
    for sl in withdraw_slots.values():
        gp.addProperty(name         = sl,
                       initialValue = 0,
                       description  = f'Withdrawn to {sl}')
    for sl in reenter_slots.values():
        gp.addProperty(name         = sl.replace(' ','_'),
                       initialValue = 0,
                       description  = f'Reenter from {sl}')
    gp.addProperty(name         = 'ReenterTurn',
                   initialValue = 0,
                   description  = 'Store reenter turn')
    

    # ================================================================
    reOpts = [chr(m) for m in range(ord('A'),ord('N'))]
    for reOpt in reOpts:
        gp.addProperty(name         = f'ReinforceOpt{reOpt}',
                       initialValue = 0,
                       isNumeric    = True,
                       description  = f'Reinforcement option {reOpt}')
    for faction in ['Axis','Allied']:
        gp.addProperty(name         = faction+vpPenalty,
                       initialValue = 0,
                       isNumeric    = True,
                       description  = f'Penalty on VPs from reinforcements')

    gp.addProperty(name = victoryPretty,
                   initialValue = '',
                   isNumeric = False,
                   description = 'Pretty format of VP conditions')
        
        


    # ================================================================
    #
    # Keys, global properties
    #
    # ----------------------------------------------------------------
    updateHex          = key(NONE,0)+',updateHex'
    checkDeclare       = key(NONE,0)+',checkDeclare'
    checkResolve       = key(NONE,0)+',checkResolve'
    checkSetup         = key(NONE,0)+',checkSetup'
    calcOddsKey        = key(NONE,0)+',wgCalcBattleOdds'
    calcIdxKey         = key(NONE,0)+',wgCalcBattleIdx'
    calcFracKey        = key(NONE,0)+',wgCalcBattleFrac'
    calcAF             = key(NONE,0)+',wgCalcBattleAF'
    calcDF             = key(NONE,0)+',wgCalcBattleDF'
    setBattleKey       = key(NONE,0)+',wgSetBattle'
    getBattleKey       = key(NONE,0)+',wgGetBattle'
    clearBattleKey     = key(NONE,0)+',wgClearBattle'
    zeroBattleDF       = key(NONE,0)+',wgZeroBattleDF'
    resetPlacedKey     = key(NONE,0)+',wgMarkBattleResetPlaced'
    dieRoll            = key(NONE,0)+',dieRoll'
    flipTurn           = key(NONE,0)+',flipTurn'
    startAxis          = key(NONE,0)+',startAxis'    
    startAllied        = key(NONE,0)+',startAllied'
    startInitiative    = key(NONE,0)+',startInitiative'
    checkC2            = key(NONE,0)+',checkC2'
    resetC2            = key(NONE,0)+',checkStatus'
    resetOOS           = key(NONE,0)+',resetOOS'
    counterCmd         = key(NONE,0)+',counterAttack'
    deleteKey          = key(NONE,0)+',delete'
    resetStep          = key(NONE,0)+',resetStep'
    toggleStep         = key(NONE,0)+',toggleStep'
    resolveKey         = key('Y')
    declareKey         = key('X')
    counterKey         = key('X',CTRL_SHIFT)
    stepKey            = key('F')
    eliminateKey       = key('E')
    captureKey         = key('C')
    captureUserKey     = key('C',CTRL_SHIFT)
    restoreKey         = key('R')
    augKey             = key('A') # key(NONE,0)+',augment'
    replaceKey         = key('I')
    combatSupplyGeneral= key('1')
    combatSupplyAttack = key('2')
    combatSupplyMaximal= key('3')
    loadKey            = key('L')
    unloadKey          = key('U')
    sacrificeKey       = key('S')
    surrenderKey       = key('S')
    withdrawKey        = key('W')
    oosKey             = key('O')
    notOosKey          = key('O',CTRL_SHIFT)
    dismountKey        = key('B')
    mountKey           = key('B',CTRL_SHIFT)
    ooC2Key            = key('Q')
    inC2Key            = key('Q',CTRL_SHIFT)
    oosCmd             = key(NONE,0)+',OutOfSupply'
    isCmd              = key(NONE,0)+',InSupply'
    captureCmd         = key(NONE,0)+',capture'
    stepCmd            = key(NONE,0)+',stepLoss'
    eliminateCmd       = key(NONE,0)+',eliminate'
    stepOrElim         = key(NONE,0)+',stepOrElim'
    updateVP           = key(NONE,0)+',updateVP'
    checkVP            = key(NONE,0)+',checkVP'
    resetStatus        = key(NONE,0)+',resetStatus'
    augStepKey         = key(NONE,0)+',augmentStep'
    augRst             = augStepKey+'Reset'
    combatSupplyReset  = key(NONE,0)+',combatSupplyReset'
    printKey           = key(NONE,0)+',print'
    autoReinforce      = 'gbAutoreinforce'
    autoC2             = 'gbAutoC2'
    debug              = 'wgDebug'
    verbose            = 'wgVerbose'
    hidden             = 'wg hidden unit'
    currentBattle      = 'wgCurrentBattle'
    battleUnit         = 'wgBattleUnit'
    battleCalc         = 'wgBattleCalc'
    battleMarker       = 'wgBattleMarker'
    battleCtrl         = 'wgBattleCtlr'
    battleNo           = 'wgBattleNo'
    battleResult       = 'wgBattleResult'
    battleShift        = 'wgBattleShift'
    battleFrac         = 'wgBattleFrac'
    battleAF           = 'wgBattleAF'
    battleDF           = 'wgBattleDF'
    gameTurn           = 'game turn'
    current            = 'Current'
    stackDx            = 6 # 'wgStackDx'
    stackDy            = 9 # 'wgStackDy'
    start1Key          = key(NONE,0)+',start1'
    start2Key          = key(NONE,0)+',start2'
    start3Key          = key(NONE,0)+',start3'
    setitupKey         = key(NONE,0)+',setitup'
    reinforceKey       = key(NONE,0)+',reinforce'
    reenterKey         = key(NONE,0)+',reenter'
    wgRollDice         = key(NONE,0)+',wgRollDice'
    rollDefaultDice    = key(NONE,0)+',wgRollDefaultDice'
    rollAltDice        = key(NONE,0)+',wgRollAltDice'
    diceKey            = key('6',ALT)
    nextPhase          = key('T',ALT)
    updateRPKey        = key(NONE,0)+',updateRPs'
    incrRPKey          = key(NONE,0)+',incrRP'
    decrRPKey          = key(NONE,0)+',decrRP'
    capRPKey           = key(NONE,0)+',capRP'
    incrRPTurnKey      = incrRPKey+'Turn'
    decrRp             = decrRPKey
    newSupply          = key(NONE,0)+',newSupply'
    checkVictoryKey    = key(NONE,0)+',checkVictory'
    countIncrKey       = key(NONE,0)+',incrCount'
    maltaKey           = key('J',ALT)
    maltaInvasionKey   = key(NONE,0)+',maltaInvasion'
    checkMaltaKey      = key(NONE,0)+',checkMalta'
    alexandriaC        = '1351'
    elAgheilaC         = '0702'
    tobrukC            = '1925'
    bardiaC            = '1730'

    objectiveNames = {
        alexandriaC: 'Alexandria',
        tobrukC:     'Tobruk',
        elAgheilaC:  'El Agheila'
    }

    # ================================================================
    #
    # Default preferences
    #
    autoOdds    = 'wgAutoOdds'
    autoResults = 'wgAutoResults'
    c2OneRoll   = 'C2OneRoll'
    go          = game.getGlobalOptions()[0]
    go.addOption(name='scenarioPropertiesIcon',   value='opt-icon.png')
    go.addOption(name='scenarioPropertiesHotKey', value=key('O',ALT))
    go.addBoolPreference(name = autoReinforce,
                         default = True,
                         desc    = 'Automatic reinforcements',
                         tab     = game['name'])
    go.addBoolPreference(name = autoC2,
                         default = True,
                         desc    = 'Automatic C2 rolls',
                         tab     = game['name'])
    prefs = go.getPreferences()    
    prefs[debug]      ['default'] = False
    prefs[verbose]    ['default'] = True
    prefs[autoOdds]   ['default'] = True
    prefs[autoResults]['default'] = True

    # ---------------------------------------------------------------
    # Optional rules as scenario properties
    genSp = gp.addScenarioTab(
        name = 'General',
        description = 'General optional rules',
        heading = ('<html><b>General option rules</b><br><br>'
                   '<blockquote>'
                   'After selecting optional rules, and locking them, '
                   'make sure to distribute Allied and Axis starting '
                   'units in the respective bases.<br>'
                   'Then go on to the next phase <code>Alt-T</code>'
                   '</blockquote></html>'))
    alliedSp = gp.addScenarioTab(
        name = 'Allied',
        description = 'Allied optional rules',
        heading     = ('<html><b>Optional rules '
                       'for the Allied faction</b>'))
    axisSp = gp.addScenarioTab(
        name = 'Axis',
        description = 'Axis optional rules',
        heading     = ('<html><b>Optional rules '
                       'for the Allied faction</b></html>'))

    genSp.addBoolean(
        name = reactionMove,
        initialValue = False,
        description = ('<html>Enable optional rule '
                       '<i>Reaction Move</i> (AHGC)'
                       '</html>'))
    alliedSp.addBoolean(
        name         = c2OneRoll,
        initialValue = False,
        description  = ('<html>One common die roll for '
                        'C<sup>2</sup> resolutions (SPI)'
                        '</html>'))
    axisSp.addBoolean(
        name = itInfantryOpt,
        initialValue = False,
        description = ('<html>Enable optional rule '
                       '<i>Italian infantry reliability</i> (AHGC)'
                       '</html>'))

    game.addStartupMassKey(
        name         = 'Show scenario options',
        hotkey       = key('O',ALT),
        target       = '',
        filter       = f'{{BasicName=="{gameTurn}"}}',
        whenToApply  = StartupMassKey.START_GAME,
        reportFormat = (f'{{{debug}?("~Show scenario options"):""}}'))
                           

    # ================================================================
    #
    # Inventory
    #
    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status. 
    filt = '{(Faction=="Allied"||Faction=="Axis")&&CurrentZone=="hexes"}'
    grp  = 'Faction,Supply_Level'
    disp = ('{PropertyValue==Faction ? Faction : '
            '(Supply_Level==1 ? "in-Supply" : "out-of-Supply")}')
    game.addInventory(include       = filt,
                      groupBy       = grp,
                      sortFormat    = '$PrettyName$',
                      leafFormat    = '$PrettyName$',
                      tooltip       = 'Show status of pieces',
                      nonLeafFormat = disp,
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'unit-icon.png')
    

    # ================================================================
    #
    # Dice
    #
    # ----------------------------------------------------------------
    # Dice
    diceName     = '1d6Dice'
    dices        = game.getSymbolicDices()
    toremove     = []
    for dn, dice in dices.items():
        # print(f'Dice: {dn}')
        if dn == diceName:
            dice['hotkey'] = diceKey

    for r in toremove:
        dices.remove(r)

    game.getPieceWindows()['Counters']['icon'] = 'unit-icon.png'
    
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    maps  = game.getMaps()
    main  = maps['Board']
    board = main['mapName']

    # ----------------------------------------------------------------
    # Delete key
    dkey = main.getMassKeys().get('Delete',None)
    if dkey is not None:
        dkey['icon'] = ''

    # ----------------------------------------------------------------
    # Layers
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    rpLayer         = 'RP'
    btlLayer        = 'Battle' # Now works due to new logic
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    objLayer        = 'Objectives'
    supLayer        = 'Supplies'
    lorLayer        = 'Lorries'

    layerNames = {objLayer:  {'t': objLayer,            'i': '' },
                  supLayer:  {'t': supLayer,            'i': '' },
                  lorLayer:  {'t': lorLayer,            'i': '' },
                  rpLayer:   {'t': rpLayer+' markers',  'i': '' },
                  unitLayer: {'t': unitLayer,           'i': '' },
                  btlLayer:  {'t': btlLayer,            'i': '' },
                  oddLayer:  {'t': oddLayer+' markers', 'i': '' },
                  resLayer:  {'t': resLayer+' markers', 'i': '' } }
                  
    layers = main.addLayers(description='Layers',
                            layerOrder=layerNames)
    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = 'layer-icon.png',
                         menuItems   = ([f'Toggle {ln["t"]}'
                                         for ln in layerNames.values()]
                                        +['Show all']))

    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        #print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        print('Hide pueces button not found',hideP)
    
    # ----------------------------------------------------------------
    # LOS
    # main.addLOS()
    
    # ----------------------------------------------------------------
    # Change stacking to be more tight since stacks may grow high
    stackmetrics = main.getStackMetrics(single=True)[0]
    stackmetrics['unexSepX'] = stackDx
    stackmetrics['unexSepY'] = stackDy
    stackmetrics['exSepX']   = int(2.5*stackDx)
    stackmetrics['exSepY']   = int(2.5*stackDy)
    main['color']            = '' # rgb(255,215,79)
    # Zoom in a bit more on the counters - can be hard to read with
    # the small font used for names.
    # ----------------------------------------------------------------
    # Detail viewer 
    dv = main.getCounterDetailViewer()[0]
    dv['summaryReportFormat'] = ('{"<b>"+'
                                 '(GetString("LocationName").length()==5?'
                                 '(GetString("LocationName")'
                                 '.substring(0,2)+'
                                 'GetString("LocationName")'
                                 '.substring(3,5)):('
                                 'GetString("LocationName").endsWith("de rps")?'
                                 '"Axis RPs":('
                                 'GetString("LocationName").endsWith("br rps")?'
                                 '"Allied RPs":'
                                 'GetString("LocationName"))))' 
                                 '+"</b>"}')
    dv['bgColor']            = rgb(0xde,0xcb,0xa2)
    dv['borderColor']        = rgb(0x33,0x33,0x33)
    dv['display']            = CounterDetailViewer.INC_LAYERS
    dv['propertyFilter']     = ''
    dv['layerList']          = ','.join([unitLayer,supLayer,lorLayer,rpLayer])
    dv['showMoveSelectde']   = True
    dv['showNoStack']        = True
    dv['showNonMovable']     = True
    dv['showTerrainBeneath'] = CounterDetailViewer.IF_ONE
    #dv['verticalOffset']     = 10

    
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    more = doc.addHelpFile(title='More information',fileName='help/more.html')
    with open('help.html','r') as h:
        moreHelp = ''.join(h.readlines())
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # tut = doc.getTutorials()

    # ==================================================================
    game.remove(maps['DeadMap'])
    
    # ==================================================================
    oobs = game.getChartWindows()['OOBs']

    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    # print(hgrid['x0'],hgrid['y0'])
    hgrid['x0'] = 102
    hgrid['y0'] = 14
    # print(f'grid offsets: h={hnum["hOff"]} v={hnum["vOff"]}')
    hnum['hOff']  = 0
    hnum['vOff']  = 0
    hnum['sep']   = '@'
    #
    # Remove grids from cadres
    #
    for faction in ['Allied','Axis']:
        zone = zones[f'{faction} cadre']
        #grid = zone.getSquareGrids()[0]
        #zone.remove(grid)

    # ================================================================
    #
    # Turn track
    #
    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Setup',                           # 0
                     'Allied supply',                   # 1
                     'Allied C2',                       # 2
                     'Allied movement',                 # 3
                     'Allied combat supply',            # 4
                     'Allied combat resolution',        # 5
                     'Allied reinforcements',           # 6
                     'Axis supply',                     # 7
                     'Axis movement',                   # 8
                     'Axis combat supply',              # 9
                     'Axis combat resolution',          # 10
                     'Axis reinforcements'              # 11
                     ]
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    turns['reportFormat'] = '--- <b><i>$newTurn$</i></b> ---'

    alSupply = phaseNames[1]
    alC2     = phaseNames[2]
    alMove   = phaseNames[3]
    alComSup = phaseNames[4]
    alCombat = phaseNames[5]
    alReinf  = phaseNames[6]
    axSupply = phaseNames[7] 
    axMove   = phaseNames[8]
    axComSup = phaseNames[9]
    axCombat = phaseNames[10]
    axReinf  = phaseNames[11]
   
    # ----------------------------------------------------------------
    # Specific actions 
    turns.addHotkey(hotkey       = setitupKey,
                    match        = f'{{Phase=="{alSupply}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'SetItUp')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Turn!=1&&Phase=="{alSupply}"}}',
                    reportFormat = '--- <b>Allied Turn</b> ---',
                    name         = 'Allied Turn')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Phase=="{axSupply}"}}',
                    reportFormat = '--- <b>Axis Turn</b> ---',
                    name         = 'Axis Turn')
    turns.addHotkey(hotkey       = reenterKey+'OOB',
                    match        = f'{{{autoReinforce}&&Phase=="{alSupply}"}}',
                    reportFormat = '--- <i>Reenter Allied faction</i> ---',
                    name         = 'Allied reenter')
    turns.addHotkey(hotkey       = reenterKey+'Cadre',
                    match        = f'{{{autoReinforce}&&Phase=="{alSupply}"}}',
                    reportFormat = '--- <i>Reenter Allied faction</i> ---',
                    name         = 'Allied reenter')
    turns.addHotkey(hotkey       = reinforceKey+'Allied',
                    match        = f'{{{autoReinforce}&&Phase=="{alReinf}"}}',
                    reportFormat = '--- <i>Reinforce Allied faction</i> ---',
                    name         = 'Allied reinforcements')
    turns.addHotkey(hotkey       = reinforceKey+'Axis',
                    match        = f'{{{autoReinforce}&&Phase=="{axReinf}"}}',
                    reportFormat = '--- <b>Reinforce Axis faction</b> ---',
                    name         = 'Axis reinforcements')
    turns.addHotkey(hotkey       = countIncrKey,
                    match        = (f'{{Phase=="{alReinf}"||'
                                    f'Phase=="{axReinf}"}}'),
                    reportFormat = (f'{{{debug}?("~Increase control conters")'
                                    f':""}}'))
    # --- Add commands to turn track to do stuff, and provide reminders
    turns.addHotkey(hotkey = checkDeclare,
                    name   = 'Check for (any) declare phase')
    turns.addHotkey(hotkey = checkResolve,
                    name   = 'Check for (any) combat phase')
    turns.addHotkey(hotkey = checkSetup,
                    match  = '{Turn==1}',
                    name   = 'Check for setup phase')
    turns.addHotkey(hotkey = nextPhase,
                    match  = '{Phase=="Setup"&&Turn>1}',
                    name   = 'Skip setup phase on later turns')
    turns.addHotkey(hotkey = checkC2,
                    match  = f'{{Phase=="{alC2}"}}',
                    name   = 'Check command and control of allied')
    turns.addHotkey(hotkey = incrRPTurnKey,
                    match  = '{Phase.contains("reinforcement")}',
                    #reportFormat = f'{{{debug}?("~New RPs"):""}}'
                    )
    turns.addHotkey(hotkey = combatSupplyReset,
                    match  = f'{{Phase=="{alReinf}"||Phase=="{axReinf}"}}',
                    reportFormat=f'{{{debug}?("~Clear combat supplies"):""}}')
    turns.addHotkey(hotkey = capRPKey,
                    match  = '{Phase.contains("supply")}',
                    reportFormat = f'{{{debug}?("~Caping RPs to 10"):""}}')
    turns.addHotkey(hotkey = checkMaltaKey,
                    match  = f'{{Phase=="{axSupply}"&&Turn==15}}',
                    reportFormat = (f'{{{debug}?("!Axis faction may "+'
                                    f'"attempt the invasion of Malta"):""}}'))
    turns.addHotkey(hotkey = resetC2,
                    match  = f'{{Phase=="{axSupply}"}}',
                    reportFormat = (f'{{{debug}?("~Clear out-of-C2"):""}}'))
    turns.addHotkey(hotkey = resetOOS+'Allied',
                    match  = f'{{Phase=="{axSupply}"}}',
                    reportFormat = (f'{{{debug}?("~Clear out-of-OO2"):""}}'))
    turns.addHotkey(hotkey = resetOOS+'Axis',
                    match  = f'{{Phase=="{alSupply}"}}',
                    reportFormat = (f'{{{debug}?("~Clear out-of-OO2"):""}}'))
    #0:no attempt,1:success,2:failure
    turns.addHotkey(hotkey = '',
                    match  = f'{{Phase=="{axMove}"&&Turn==15}}',
                    reportFormat = (f'{{{verbose}?({maltaStatus}>0?'
                                    f'"`Axis units may not move into Eqypt":'
                                    f'"?No Axis attempt at Malta invasion"):'
                                    f'""}}'))
    # Make special case of Malta Invasion 

    # ----------------------------------------------------------------
    # Turn track progress 
    turnp = addTurnPrototypes(20,turns,main,prototypeContainer,
                              alSupply, #phaseNames[0],
                              marker=gameTurn, zoneName='Turn track')

    # --- Clear markers phases ---------------------------------------
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="{alSupply}"'
        f'||Phase=="{axSupply}"'
        f'}}')
    # Do not clear moved markers between movement and combat, so that
    # we may check if `Moved` of supply units have been set.
    moved = keys['Clear moved markers']
    phased = '||'.join([f'Phase=="{p}"' for p in phaseNames
                        if p not in [alComSup,alCombat,
                                     axComSup,axComSup]])
    moved['match'] = f'{{{phased}}}'

    # ================================================================
    #
    # Global key commands
    #
    # ----------------------------------------------------------------
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
    keys                     = main.getMassKeys()
    userMark                 = keys['User mark battle']
    selMark                  = keys['Selected mark battle']
    resMark                  = keys['Selected resolve battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = globalDeclare
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = globalDeclare
    resMark ['canDisable']   = True
    resMark ['propertyGate'] = globalResolve
    
    # ----------------------------------------------------------------
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    main.addMassKey(name         = 'Flip turn marker',
                    buttonHotkey = flipTurn,
                    hotkey       = stepKey,
                    buttonText   = '', 
                    target       = '',
                    singleMap    = True,
                    filter       = f'{{BasicName == "{gameTurn}"}}')
    main.addMassKey(name         = 'Reinforce Allied faction',
                    buttonHotkey = reenterKey+'OOB',
                    hotkey       = reenterKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{Faction == "Allied"&&'
                                    f'CurrentZone=="Allied OOB"}}'),
                    reportFormat = 'Reenter Allied - global'
                    )
    main.addMassKey(name         = 'Reinforce Allied faction',
                    buttonHotkey = reenterKey+'Cadre',
                    hotkey       = reenterKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{Faction == "Allied"&&'
                                    f'CurrentZone=="Allied cadre"}}'),
                    reportFormat = 'Reenter Allied - from cadre'
                    )
    main.addMassKey(name         = 'Reinforce Allied faction',
                    buttonHotkey = reinforceKey+'Allied',
                    hotkey       = reinforceKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = f'{{Faction == "Allied"}}',
                    reportFormat = 'Reinforce Allied - global'
                    )
    main.addMassKey(name         = 'Reinforce Axis faction',
                    buttonHotkey = reinforceKey+'Axis',
                    hotkey       = reinforceKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = f'{{Faction == "Axis"}}',
                    #reportFormat = 'Reinforce US'
                    )
    main.addMassKey(name         = 'Increase control counts',
                    buttonHotkey = countIncrKey,
                    hotkey       = countIncrKey,
                    buttonText   = '',
                    singleMap    = True,
                    target       = '',
                    filter       = '{BasicName=="small control"}')
    main.addMassKey(name         = 'New RPs',
                    buttonHotkey = incrRPTurnKey,
                    hotkey       = incrRPTurnKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = True,
                    filter       = '{RP==true&&Phase.contains(Faction)}',
                    #reportFormat = f'{{{debug}?("~New RPs, global"):""}}'
                    )
    main.addMassKey(name         = 'Cap RPs',
                    buttonHotkey = capRPKey,
                    hotkey       = capRPKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = True,
                    filter       = '{RP==true}',
                    #reportFormat = f'{{{debug}?("~Cap RPs"):""}}'
                    )
    main.addMassKey(name         = 'Augument allied unit',
                    buttonHotkey = augKey,
                    hotkey       = augKey,
                    buttonText   = '',
                    singleMap    = True,
                    #reportFormat= (f'{{{debug}?("~Augment allied unit"):""}}')
                    )
    main.addMassKey(name         = 'Replace unit from cadre',
                    buttonHotkey = replaceKey,
                    hotkey       = replaceKey,
                    buttonText   = '',
                    singleMap    = True,
                    reportFormat= (f'{{{debug}?("~Replace unit"):""}}')
                    )
    main.addMassKey(name         = 'Check for malta invasion',
                    buttonHotkey = checkMaltaKey,
                    hotkey       = checkMaltaKey,
                    buttonText   = '',
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}')
    # ----------------------------------------------------------------
    # Combat supply
    main.addMassKey(name         = 'Combat supply',
                    buttonHotkey = combatSupplyGeneral,
                    hotkey       = combatSupplyGeneral,
                    buttonText   = '',
                    singleMap    = True,
                    filter       = '{IsAttacker}')
    main.addMassKey(name         = 'Combat supply',
                    buttonHotkey = combatSupplyAttack,
                    hotkey       = combatSupplyAttack,
                    buttonText   = '',
                    singleMap    = True,
                    filter       = '{IsAttacker}')
    main.addMassKey(name         = 'Combat supply',
                    buttonHotkey = combatSupplyMaximal,
                    hotkey       = combatSupplyMaximal,
                    buttonText   = '',
                    singleMap    = True,
                    filter       = '{IsAttacker}')
    main.addMassKey(name         = 'Combat supply',
                    buttonHotkey = combatSupplyReset,
                    hotkey       = combatSupplyReset,
                    buttonText   = '',
                    singleMap    = True,
                    target       = '',
                    filter       = '{IsAttacker}',
                    reportFormat = (f'{{{debug}?('
                                    f'"?Resetting combat supply"):""}}'))
    # ----------------------------------------------------------------
    # Forward keys
    fwdKeys = [
        #resolveKey,        # key('Y')
        #declareKey,        # key('X')
        counterKey,         # key('X',CTRL_SHIFT)
        #stepKey,           # key('F')
        #eliminateKey,      # key('E')
        captureUserKey,     # key('C')
        #restoreKey,        # key('R')
        augKey,             # key('A') # key(NONE,0)+',augment'
        replaceKey,         # key('I')
        combatSupplyGeneral,# key('1')
        combatSupplyAttack, # key('2')
        combatSupplyMaximal,# key('3')
        loadKey,            # key('L')
        unloadKey,          # key('U')
        sacrificeKey,       # key('S')
        withdrawKey,        # key('W')
        oosKey,             # key('O')
        notOosKey,          # key('O',CTRL_SHIFT)
        dismountKey,        # key('B')
        mountKey,           # key('B',CTRL_SHIFT)
        ooC2Key,
        inC2Key
    ]+[key(str(i),CTRL_SHIFT) for i in range(1,4)]
    fwdbut = {}
    for k in fwdKeys:
        fwdbut[k] = main.addMassKey(name         = 'Forward key to selected',
                                    buttonHotkey = k,
                                    hotkey       = k,
                                    buttonText   = '',
                                    singleMap    = True)
    fwdbut[oosKey]['icon']            = 'oos-icon.png'
    fwdbut[oosKey]['tooltip']         = 'Mark unit Out-of-Supply'
    fwdbut[ooC2Key]['icon']           = 'c2-icon.png'
    fwdbut[ooC2Key]['tooltip']        = 'Mark unit Out-of-C2'
    fwdbut[ooC2Key]['canDisable']     = True
    fwdbut[ooC2Key]['propertyGate']   = f'{{{autoC2}}}'
    fwdbut[dismountKey]['icon']       = 'dismount-icon.png'
    fwdbut[dismountKey]['tooltip']    = 'Mark unit as dismounted'
                    
    # ----------------------------------------------------------------
    # - Check for declaration phases 
    main.addMassKey(name         = 'Check for declare phase',
                    buttonHotkey = checkDeclare,
                    buttonText   = '',
                    hotkey       = checkDeclare,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for declare"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkResolve,
                    buttonText   = '',
                    hotkey       = checkResolve,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for resolve"):""}}'))
    # ----------------------------------------------------------------
    # - Store defender hex
    # repRough = '+","+'.join([f'{defenderRough}{no}' for no in range(1,7)])
    # repSwamp = '+","+'.join([f'{defenderSwamp}{no}'  for no in range(1,7)])
    # repFort  = '+","+'.join([f'{defenderFort}{no}'  for no in range(1,7)])
    repDef   = '+","+'.join([f'{defenderHex}{no}' for no in range(1,7)])
    repEdg   = '+","+'.join([f'{overEscarpment}{no}' for no in range(1,7)])
    main.addMassKey(name         = 'Store defender hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curDef,
                    reportFormat = (f'{{{debug}?("Mass update defender hex"):""}}'))
    main.addMassKey(name         = 'Store attacker hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curAtt,
                    reportFormat = (f'{{{debug}?("Mass update attacker hex"):""}}'))
    # ----------------------------------------------------------------
    main.addMassKey(name         = 'Set it up',
                    buttonHotkey = setitupKey,
                    buttonText   = '',
                    hotkey       = setitupKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{BasicName=="{gameTurn}"}}'),
                    reportFormat = (f'{{{debug}?("~ Set-up pieces"):""}}'))
    # ----------------------------------------------------------------
    main.addMassKey(name         = 'Reset C2',
                    buttonHotkey = resetC2,
                    buttonText   = '',
                    hotkey       = resetC2,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{Faction=="Allied"&&CurrentZone=="hexes"}}'),
                    reportFormat = (f'{{{debug}?("~ Reset C2"):""}}'))
    main.addMassKey(name         = 'Reset Allied OOS',
                    buttonHotkey = resetOOS+'Allied',
                    buttonText   = '',
                    hotkey       = resetOOS,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{Faction=="Allied"&&CurrentZone=="hexes"}}'),
                    reportFormat = (f'{{{debug}?("~ Reset Allied OOS"):""}}'))
    main.addMassKey(name         = 'Reset Axis OOS',
                    buttonHotkey = resetOOS+'Axis',
                    buttonText   = '',
                    hotkey       = resetOOS,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{Faction=="Axis"&&CurrentZone=="hexes"}}'),
                    reportFormat = (f'{{{debug}?("~ Reset Axis OOS"):""}}'))
    main.addMassKey(name         = 'Check C2',
                    buttonHotkey = checkC2+'Real',
                    buttonText   = '',
                    hotkey       = checkC2,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{Faction=="Allied"&&CurrentZone=="hexes"}}'),
                    reportFormat = (f'{{{debug}?("~ Check C2, individual"):""}}'))
    main.addMassKey(name         = 'Check C2',
                    buttonHotkey = checkC2,
                    buttonText   = '',
                    hotkey       = checkC2,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{BasicName=="{gameTurn}"}}'),
                    reportFormat = (f'{{{debug}?("~ Check C2"):""}}'))
    for faction, phase in zip(['Axis','Allied'],
                              [axReinf, alReinf]):
        turns.addHotkey(hotkey       = newSupply+faction,
                        match        = f'{{Phase=="{phase}"}}',
                        #reportFormat = f'--- <b>{faction} supply</b> ---',
                        name         = f'{faction} supplies')
        main.addMassKey(
            name = 'Take new supply unit',
            buttonText   = '', # f'{faction} supply',
            buttonHotkey = newSupply+faction,
            hotkey       = reinforceKey+'Supply',
            singleMap    = False,
            target       = '',
            filter       = (f'{{Faction=="{faction}"&&'
                            f'{supplyTaken}<=0&&'
                            f'Type=="supply"&&'
                            f'CurrentBoard=="{faction} OOB"}}'),
            reportFormat = (f'{{{debug}?("~{faction} new supply"):""}}'))
        
    # ================================================================
    rpDecrKeyGen = lambda nation,tpe,cnt : decrRPKey+nation+tpe+str(cnt)
    rpPiece = {
        'Allied': {
            'Inf': 'br i rp',
            'Arm': 'br a rp',
        },
        'German': {
            'Inf': 'de i rp',
            'Arm': 'de a rp',
        },
        'Italian': {
            'Inf': 'it i rp',
            'Arm': 'it a rp'
        }
    }
        
    for nation,tps in rpPiece.items():
        for tpe,pcs in tps.items():
            for n in range(1,6):
                k = rpDecrKeyGen(nation,tpe,n)
                main.addMassKey(
                    name         = f'RP for {nation} {tpe} -{n}',
                    buttonHotkey = k,
                    buttonText   = '',
                    hotkey       = k,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{BasicName=="{pcs}"}}'),
                    reportFormat = (f'{{{debug}?("~Decrement "+'
                                    f'"{nation}{tpe} RPs by {n} "+'
                                    f'"{pcs}"):""}}'))

                
    # ================================================================
    # VPs
    main.addMassKey(
        name = 'Evaluate victory points',
        buttonText = '',
        buttonHotkey = key('V',ALT),
        hotkey       = checkVictoryKey,
        icon         = 'victory-icon.png',
        tooltip      = 'Calculate current VPs',
        singleMap    = True,
        target       = '',
        filter       = (f'{{CurrentBoard=="{board}"&&'
                        f'BasicName=="{gameTurn}"}}'),
        reportFormat = (f'{{{debug}?("~Check victory points"):""}}'))
    main.addMassKey(
        name         = 'Evaluate victory points',
        buttonText   = '',
        buttonHotkey = checkVictoryKey+'Show',
        hotkey       = '',
        singleMap    = True,
        target       = '',
        filter       = '{false}',
        reportFormat = (f'{{"`<table>"+'
                        f'{victoryPretty}.replace("``","<tr><td colspan=2>")'
                        f'.replace("`","<tr><td>")'
                        f'.replace("@","</td></tr>")'
                        f'.replace("||","</td><td colspan=2>")'
                        f'.replace("|","</td><td>")+'
                        f'"</table>"}}')
        # reportFormat = f'{{{victoryPretty}}}'
    )
    for hx,name in objectiveNames.items():
        gp.addProperty(name         = name.replace(' ','')+'Control',
                       initialValue = 'Axis' if hx==elAgheilaC else 'Allied',
                       description  = f'Controller of {name}')
        gp.addProperty(name         = name.replace(' ','')+'Count',
                       initialValue = 0,
                       isNumeric    = True,
                       description  = (f'Consequtive player turns of '
                                       f'controling {name}'))
    # ================================================================
    # VPs
    main.addMassKey(
        name         = 'possible Malta invasion',
        buttonText   = '',
        buttonHotkey = maltaKey,
        hotkey       = maltaInvasionKey,
        icon         = 'malta-icon.png',
        tooltip      = 'Axis attempt invasion of Malta',
        singleMap    = True,
        target       = '',
        canDisable   = True,
        propertyGate = maltaNotPossible,
        filter       = (f'{{BasicName=="{gameTurn}"}}'),
        reportFormat = (f'{{{debug}?("~Attempting invasion of Malta"):""}}'))
    main.addMassKey(
        name         = 'move Malta invasion',
        buttonText   = '',
        buttonHotkey = maltaInvasionKey+'Move',
        hotkey       = maltaInvasionKey+'Move',
        icon         = '',
        tooltip      = '',
        singleMap    = False,
        target       = '',
        filter       = (f'{{BasicName=="de malta"}}'),
        reportFormat = (f'{{{debug}?("~Move Malta marker"):""}}'))

    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    prototypes         = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        if p['name'] == ' prototype':
            prototypes.remove(p)
        if p['name'] in ['artillery reconnaissance prototype']:
            prototypes.remove(p)
                         
        if p['name'] in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(p['name'])

    prototypes     = prototypeContainer.getPrototypes(asdict=True)

    # ----------------------------------------------------------------
    # Modify the types prototypes to contain the main type trait
    mainTypes = {'infantry motorized':      'Infantry',
                 'infantry':                'Infantry',
                 'reconnaissance':          'Infantry',
                 'armoured reconnaissance': 'Armoured',
                 'armoured infantry':       'Armoured',
                 'armoured':                'Armoured',
                 'infantry airborne':       'Infantry',
                 'infantry air assault':    'Infantry'
                }
    revTypes = {'Infantry': [],
                'Armoured': [] }
    for k,v in mainTypes.items():
        revTypes[v].append(k)
                 
    for mainType, realType in mainTypes.items():
        prototype = prototypes.get(f'{mainType} prototype',None)
        if prototype is None :
            print(f'Missing {mainType} prototype')
            continue
        
        traits = prototype.getTraits()
        basic  = traits.pop()
        traits.append(MarkTrait(name='MainType', value=realType))
        prototype.setTraits(*traits,basic)

    # ----------------------------------------------------------------
    currentB = prototypes[currentBattle]
    traits   = currentB.getTraits()
    basic    = traits.pop()

    # traits.extend([
    #     GlobalPropertyTrait(
    #         ['',setBattleKey,GlobalPropertyTrait.DIRECT,
    #          '{ThisCounterAttack}'],
    #         name = fromCounter),
    #     DynamicPropertyTrait(
    #         ['',getBattleKey,DynamicPropertyTrait.DIRECT,
    #          f'{{{fromCounter}}}'],
    #         name = 'ThisCounterAttack',
    #         numeric = True,
    #         value = False),
    #     ReportTrait(
    #         getBattleKey,
    #         report = (f'{{{debug}?("~"+BasicName+'
    #                   f'"Get {fromCounter}="+{fromCounter}+"->"'
    #                   f'ThisCounterAttack):""}}')),
    #     ReportTrait(
    #         getBattleKey,
    #         report = (f'{{{debug}?("~"+BasicName+'
    #                   f'"Set {fromCounter} "+ThisCounterAttack+"->"'
    #                   f'{fromCounter}):""}}')),
    # ])
    traits.append(basic)
    currentB.setTraits(*traits)
    
    # ----------------------------------------------------------------
    # Modify Markers prototype
    markersP     = prototypes['Markers prototype']
    traits       = markersP.getTraits()
    basic        = traits.pop()
    traits.extend([
        DeleteTrait(name='', key=deleteKey),
        basic])
    #mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    #mdel['key']  = deleteKey
    #mdel['name'] = ''        
    markersP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Modify ControlMarkers prototype
    markersP     = prototypes['ControlMarkers prototype']
    traits       = markersP.getTraits()
    basic        = traits.pop()
    #mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    #mdel['key']  = deleteKey
    #mdel['name'] = ''
    traits = [MarkTrait(name = pieceLayer,  value = objLayer),
              NoStackTrait(select      = NoStackTrait.NORMAL_SELECT,
                           bandSelect  = NoStackTrait.NEVER_BAND_SELECT,
                           move        = NoStackTrait.NEVER_MOVE,
                           canStack    = False,
                           ignoreGrid  = False,
                           description = 'Restrict movement of objective'),
              basic]
    markersP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Create sea movement prototype
    seaMove      = key(NONE,0)+',bySea'
    ports        = [ (tobrukC,     'Tobruk'),
                     (alexandriaC, 'Alexandria'),
                     (bardiaC,     'Bardia') ]
    traits = [
        # Send to a port 
        TriggerTrait(
            name       = f'Send to {n} {h}',
            command    = f'To {n}',
            key        = key(str(i+1),CTRL_SHIFT),
            actionKeys = [seaMove+str(i+1)],
            property   = (f'{{Moved!=true&&Phase.contains("movement")&&('
                          f'{"||".join([f"LocationName==\"{hexName(o[0])}\""
                                        for o in ports if o[0] != h])}'
                          f')&&Phase.contains(Faction)&&Flipped!=true}}'))
        for i, (h, n) in enumerate(ports)
    ]
    traits.extend([
        SendtoTrait(
            mapName     = board,
            boardName   = board,
            name        = '',
            key         = seaMove+str(i+1),
            restoreName = '',
            restoreKey  = '',
            destination = SendtoTrait.GRID,
            position    = hexName(h))
        for i, (h, n) in enumerate(ports)])
    traits.extend([
        SubMenuTrait(
            subMenu = 'By Sea',
            keys    = [f'To {n}' for h, n in ports]),
        BasicTrait()])
    prototypeContainer.addPrototype(
        name        = f'Sea prototype',
        description = f'Sea prototype',
        traits      = traits)
    
    # ----------------------------------------------------------------
    # Create C2 prototype
    traits = [
        LayerTrait(
            ['',
             'no-c2-mark.png'],
            ['',
             'Out of C2 +'],
            activateName = '',
            activateMask = '',
            activateChar = '',
            increaseName = '', # 'No C2'
            increaseMask = '',
            increaseChar = '',
            decreaseName = '',
            decreaseMask = '',
            decreaseChar = '',
            resetName    = '', # 'C2 restored'
            resetKey     = resetStatus,
            under        = False,
            underXoff    = 0,
            underYoff    = 0,
            loop         = False,
            name         = 'C2', #'C2Status',
            description  = '(Un)Mark unit out of command and control',
            always       = False,
            activateKey  = '',
            increaseKey  = '', # key('2'),
            decreaseKey  = '',
            scale        = 1,
            follow       = True,
            expression   = '{C2Status}'
        ),
        # Last digit of location
        CalculatedTrait(
            name = 'C2Last',
            expression =('{CurrentZone=="hexes"?'
                         '(GetString("LocationName").substring(4)):"-"}')),
        # Check if roll or roll+2 or (roll+6)%10 is equal to
        # last digit of location
        DynamicPropertyTrait(
            ['',checkC2+'Real',DynamicPropertyTrait.DIRECT,
             (f'{{((CurrentZone=="hexes")&&('
              f'({c2OneRoll}==true?'
              f'({c2LastDigits}.contains(":"+C2Last+":")):'
              f'(Random(6)==1))))?2:1}}')],
            ['',checkC2+'Force',DynamicPropertyTrait.DIRECT, '{2}'],
            ['',resetC2,DynamicPropertyTrait.DIRECT,'{1}'],
            name = 'C2Status',
            numeric = True,
            value = 1),
        RestrictCommandsTrait(
            name          = 'Restrict C2',
            hideOrDisable = RestrictCommandsTrait.HIDE,
            expression    = (f'{{{autoC2}==true}}'),
            keys          = [ooC2Key,inC2Key]),
        TriggerTrait(
            name       = 'User sets out-of-C2',
            command    = 'Out-of-C2',
            key        = ooC2Key,
            actionKeys = checkC2+'Force',
            property   = f'{{{autoC2}!=true&&Phase=="{alC2}"}}'),
        TriggerTrait(
            name       = 'User sets in-C2',
            command    = 'In C2',
            key        = inC2Key,
            actionKeys = checkC2+'Reset',
            property   = f'{{{autoC2}!=true&&Phase=="{alC2}"}}'),
        # Register hex
        GlobalPropertyTrait(
            ['',checkC2+'Register',GlobalPropertyTrait.DIRECT,
             f'{{{c2Hexes}+":"+LocationName+":"}}'],
            name = c2Hexes,
            description = 'Register hex in C2 registery'),
        # Run the above three commands 
        TriggerTrait(
            name = 'Check C2 status',
            command    = '',
            key        = checkC2,
            actionKeys = [# checkC2+'Last',
                checkC2+'Register',
                checkC2+'Real',
                checkC2+'Maybe'],
            # Optional?
            property = f'{{!{c2Hexes}.contains(":"+LocationName+":")}}'
        ),
        TriggerTrait(
            name       = 'Propagate C2 to others in same hex',
            command    = '',
            key        = checkC2+'Maybe',
            actionKeys = [checkC2+'Propagate'],
            property = '{C2Status==2}'),
        TriggerTrait(
            name       = 'Propagate C2 to others in same hex',
            command    = '',
            key        = checkC2+'Maybe',
            actionKeys = [checkC2+'Clear'],
            property = '{C2Status==1}'),
        GlobalCommandTrait(
            commandName  = '',
            key          = checkC2+'Propagate',
            globalKey    = checkC2+'Force',
            ranged       = True,
            range        = 0,
            #rangeProperty = '{C2Status!=2}'
        ),
        GlobalCommandTrait(
            commandName  = '',
            key          = checkC2+'Clear',
            globalKey    = resetC2,
            ranged       = True,
            range        = 0,
            #rangeProperty = '{C2Status!=2}'
        ),
        ReportTrait(
            checkC2,
            report = (f'{{{debug}?("~"+BasicName+'
                      f'" {c2OneRoll}: "+{c2OneRoll}+'
                      f'" {c2LastDigits}: "+{c2LastDigits}+'
                      f'" C2Last: "+C2Last'
                      f'):""}}')),
        ReportTrait(
            checkC2+'Real',checkC2+'Force',
            report = (f'{{{verbose}?((C2Status==2)?('
                      f'"`"+PrettyName+" at "+LocationName+" ("+'
                      f'C2Last+") Out of C2 (die roll "+'
                      f'C2Roll+")"):""):""}}')),
        ReportTrait(
            checkC2+'Propagate',checkC2+'Maybe',checkC2+'Clear',
            report = (f'{{{debug}?("~"+BasicName+" @ "+LocationName+'
                      f'" Propagate C2 to others "+C2Status):""}}')),
        ReportTrait(
            resetC2,
            report = (f'{{{debug}?("~"+BasicName+" @ "+LocationName+'
                      f'" reset C2 "+C2Status):""}}')),
        ReportTrait(
            checkC2+'Register',
            report = (f'{{{debug}?("~"+BasicName+" @ "+LocationName+'
                      f'" Register hex "+{c2Hexes}):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(
        name        = f'C2 prototype',
        description = f'C2 prototype',
        traits      = traits)
    
    # ----------------------------------------------------------------
    # Create OOS prototypes
    for faction in ['Axis', 'Allied']:
        supphase  = alSupply if faction == 'Allied' else axSupply
        reiphase  = alReinf  if faction == 'Allied' else axReinf
        osupphase = axSupply if faction == 'Allied' else alSupply
        omovphase = axMove   if faction == 'Allied' else alMove
        
        traits = [
            TriggerTrait(
                name    = 'Mark as out of supply',
                command = 'Out-of-Supply',
                key     = oosKey,
                actionKeys = [oosCmd],
                property   = (f'{{Supply_Level==1&&(Phase=="{supphase}"'
                              f'||((Phase=="{osupphase}"'
                              f'||Phase=="{omovphase}")'
                              f'&&!IsAttacker))}}')),
            TriggerTrait(
                name       = 'Mark as in supply',
                command    = 'In-supply',
                key        = notOosKey,
                actionKeys = [isCmd],
                property   = (f'{{Supply_Level==2&&(Phase=="{supphase}"'
                              f'||((Phase=="{osupphase}"'
                              f'||Phase=="{omovphase}")'
                              f'&&!IsAttacker))}}')),
            TriggerTrait(
                name       = 'Reset OOS',
                command    = '',
                key        = resetOOS,
                actionKeys = [isCmd],
                property   = f'{{Supply_Level==2}}'),
            LayerTrait(['',
                        'isolated-mark.png'],
                       ['',
                        'Out-of-supply +'],
                       activateName = '',
                       activateMask = '',
                       activateChar = '',
                       increaseName = '',
                       increaseMask = '',
                       increaseChar = '',
                       decreaseName = '',
                       decreaseMask = '',
                       decreaseChar = '',
                       resetName    = '',
                       resetKey     = resetStatus,
                       under        = False,
                       underXoff    = 0,
                       underYoff    = 0,
                       loop         = True,
                       name         = 'Supply',
                       description  = '(Un)Mark unit as out-of-supply',
                       always       = True,
                       activateKey  = '',
                       increaseKey  = oosCmd,
                       decreaseKey  = isCmd, # key('I',CTRL_SHIFT),
                       scale        = 1),
        ]
        traits.append(BasicTrait())
        prototypeContainer.addPrototype(
            name        = f'{faction} OOS prototype',
            description = f'{faction} OOS prototype',
            traits      = traits)
    
    # ----------------------------------------------------------------
    # Create Lorry prototype
    loadCmd   = key(NONE,0)+',loadSupply'
    unloadCmd = key(NONE,0)+',unloadSupply'
    for faction in ['Axis','Allied']:
        mov       = alMove if faction == 'Allied' else axMove
        cap       = ([axMove, axCombat] if faction == 'Allied' else
                     [alMove, alCombat])
        capPhases = '||'.join([f'Phase=="{p}"' for p in cap])
        destName  = alexandriaC if faction == 'Allied' else elAgheilaC
        destName  = hexName(destName)
        traits = [
            MarkTrait(name = 'Faction',   value = faction),
            MarkTrait(name = pieceLayer,  value = lorLayer),
            MarkTrait(name = 'MainType',  value = 'Transport'),
            MovedTrait(),
            PrototypeTrait(name = f'{faction} OOS prototype'),
            CalculatedTrait(name = 'Flipped',
                            expression = '{Supply_Level==2}'),
            # Must come before battle unit prototype to override 
            CalculatedTrait(name = 'DF',
                            # Count how may other units have a 'DF'
                            # value - regular units do!
                            #  {CountLocation("{MainType!="Transport"}")>0?0:1}
                            expression = (
                                r'{CountLocation("{MainType!=\"Transport\"}")'
                                '>1?0:1}')),
            CalculatedTrait(name = 'EffectiveDF', expression = '{DF}'),
            MarkTrait(name = "CurrentCF",     value = "0"),
            MarkTrait(name = "CF",            value = "0"),
            MarkTrait(name = "EffectiveAF",   value = "0"),
            MarkTrait(name = "CounterFactor", value = "1"),
            # Perhaphs here
            MatTrait(name = 'Lorry', description = 'Lorry'),
            # Try to make the lorry stack-able.  Must come after mat
            # trait to not prevent the mat from working properly.
            # However, then the stacking enforcement doesn't work, so
            # there's really no way to do this right.
            #
            # NoStackTrait(select      = NoStackTrait.NORMAL_SELECT,
            #              bandSelect  = NoStackTrait.NORMAL_BAND_SELECT,
            #              move        = NoStackTrait.NORMAL_MOVE,
            #              canStack    = True,
            #              ignoreGrid  = False,
            #              description = 'Stack lorries'),
            # _must_ come after effective af and df, otherwise override
            PrototypeTrait(name = f'{battleUnit}'),
            TriggerTrait(
                command    = 'Load supply',
                key        = loadKey,
                actionKeys = [loadCmd],
                property   = f'{{Phase=="{mov}"}}'),
            TriggerTrait(
                command    = 'Unload supply',
                key        = unloadKey,
                actionKeys = [unloadCmd],
                property   = f'{{Phase=="{mov}"}}'),
            GlobalCommandTrait(
                commandName = '',
                key         = loadCmd,
                globalKey   = loadCmd,
                properties  = (f'{{Type=="supply"&&'
                               f'Faction=="{faction}"}}'),
                ranged      = True,
                range       = 0,
                deckSelect  = 1),
            GlobalCommandTrait(
                commandName   = '',
                key           = unloadCmd,
                globalKey     = unloadCmd,
                properties    = (f'{{Type=="supply"}}'),
                ranged        = True,
                range         = 0,
                deckSelect    = 1,
                rangeProperty = '{Flipped!=true}'
            ),
            # --- Reinforce ---
            SubMenuTrait(
                subMenu = 'Supply',
                keys    = ['Load supply',
                           'Unload supply',
                           'Capture or eliminate',
                           'Capture']),            
            SubMenuTrait(
                subMenu = 'Status',
                keys    = ['Out-of-Supply',
                           'In-supply']),            
            SubMenuTrait(
                subMenu = 'Combat',
                keys    = ['Declare battle',
                           'Eliminate']),            
            # --- Eliminate ---
            SendtoTrait(name        = '',
                        key         = eliminateCmd+'Real',
                        restoreKey  = '',
                        restoreName = '', 
                        mapName     = f'{faction} OOB', 
                        boardName   = f'{faction} OOB',
                        destination = SendtoTrait.REGION,
                        region      = f'{{"Turn "+(Turn+2)}}',
                        description = f'Send to OOB {faction} OOB'),
            DynamicPropertyTrait(
                ['',eliminateCmd+'Turn',DynamicPropertyTrait.DIRECT,
                 '{Turn+2}'],
                name = 'ReturnTurn',
                numeric = True,
                value   = 0),
            TriggerTrait(
                command = 'Eliminate',
                key     = eliminateKey,
                actionKeys = [unloadKey,
                              eliminateCmd+'Turn',
                              eliminateCmd+'Real'],
                property   = f'{{{capPhases}}}'),
            ReportTrait(
                eliminateKey,
                report = (f'{{{verbose}?("`"+PrettyName+'
                          f'" eliminated, return on turn "+(Turn+2)):""}}')),
            # --- Reinforce --- 
            SendtoTrait(name        = '',
                        key         = reinforceKey+'Real',
                        restoreKey  = '',
                        restoreName = '',
                        mapName     = board,
                        boardName   = board,
                        destination = SendtoTrait.GRID,
                        position    = destName,
                        description = 'Reinforce lorry'),
            TriggerTrait(
                name       = 'Reinforce',
                command    = '',
                key        = reinforceKey,
                actionKeys = [reinforceKey+'Real'],
                property   = f'{{Turn==ReturnTurn}}'),
            ReportTrait(
                reinforceKey+'Real',
                report = (f'{{{verbose}?("!"+PrettyName+" reinforce at "+'
                          f'"{destName} from"+OldLocationName+'
                          f'" on turn "+Turn):""}}')),
            #
            TrailTrait(),
            SubMenuTrait(subMenu='Various',
                         keys    = ['Mark moved',
                                    'Movement Trail']),            
            # --- Print ----
            # TriggerTrait(
            #     command = 'Print',
            #     key     = printKey),
            # ReportTrait(
            #     printKey,
            #     report = (f'{{{verbose}?("!"+PrettyName+" @ ."+'
            #               f'LocationName+". was ."+'
            #               f'OldLocationName+". return on turn "+'
            #               f'ReturnTurn):""}}')),
            # --- Reports ----
            ReportTrait(
                loadKey,
                report = (f'{{{verbose}?("!"+PieceName+" ("+'
                          f'MatID+") loads supply -> "+MatNumCargo):""}}')),
            ReportTrait(
                unloadKey,
                report = (f'{{{verbose}?("!"+PieceName+" ("+'
                          f'MatID+") unloads supply -> "+MatNumCargo):""}}')),
            BasicTrait()
        ]
        prototypeContainer.addPrototype(
            name        = f'{faction} lorry prototype',
            description = f'{faction} lorry prototype',
            traits      = traits)

    # ----------------------------------------------------------------
    # Create supply prototype
    offsetCmd   = key(NONE,0)+',supplyOffset'
    deoffsetCmd = key(NONE,0)+',supplyDeoffset'
    checkOffset = key(NONE,0)+',supplyCheckOffset'
    setOffset   = key(NONE,0)+',supplySetOffset'
    resetOffset   = key(NONE,0)+',supplyResetOffset'
    for faction in ['Axis','Allied']:
        destName  = alexandriaC if faction == 'Allied' else elAgheilaC
        destName  = hexName(destName)
        other     = 'Axis' if faction == 'Allied' else 'Allied'
        # When we can capture enemy supply - either at movement (move
        # over or overrun) or combat (advance or overrun)
        cap       = ([axMove, axCombat] if faction == 'Allied' else
                     [alMove, alCombat])
        # Voluntarily eliminate - combat support or combat
        # the latter to allow the supply to stay for full-combat support
        vol       = ([alComSup,alCombat] if faction == 'Allied' else
                     [axComSup,axCombat])
        capPhases = '||'.join([f'Phase=="{p}"' for p in cap])
        volPhases = '||'.join([f'Phase=="{p}"' for p in vol])
        reinf    = alReinf if faction == 'Allied' else axReinf
        extra    = ('' if faction == 'Allied' else
                    f'&&!((Turn==15||Turn==16)&&{maltaStatus}==2)')
        limSac   = '(Turn!=1)&&' if faction == 'Allied' else ''
        # extra is specifc to axis faction - if malta failes (2),
        # then there's no supplies on turns 15 and 16
        traits   = [
            # General traits 
            MarkTrait(name = 'Faction', value = faction),
            MarkTrait(name = pieceLayer,  value = supLayer),
            MovedTrait(),
            PrototypeTrait(name = 'Sea prototype'),
            # Cargo traits 
            CargoTrait(description        = 'Supply as cargo',
                       attachKey          = loadCmd+'Real',
                       detachKey          = unloadCmd+'Real',
                       detectionDistanceX = 30,
                       detectionDistanceY = 30),
            TriggerTrait(name        = '',
                         command     = '',#'Load',
                         key         = loadCmd,
                         actionKeys  = [checkOffset,loadCmd+'Real']),
            TriggerTrait(name = '',
                         command     = '',#'Unload',
                         key         = unloadCmd,
                         actionKeys  = [unloadCmd+'Real']),
            TriggerTrait(name = '',
                         command = 'Offset',
                         key = checkOffset,
                         property = '{WasOffset==0}',
                         actionKeys = [setOffset,offsetCmd]),
            TriggerTrait(name = '',
                         command = 'Center',
                         key = '',
                         property = '{WasOffset==1}',
                         actionKeys = [deoffsetCmd,resetOffset]),
            SubMenuTrait(
                subMenu = 'Clutter',
                keys    = ['Offset','Center']),
            # Set that we've added the offset on this piece 
            DynamicPropertyTrait(
                ['',setOffset,DynamicPropertyTrait.DIRECT, '{1}'],
                ['',resetOffset,DynamicPropertyTrait.DIRECT, '{0}'],
                name = 'WasOffset',
                numeric = True,
                value   = 0,
                description = '1 when moved off, prevents more moves'),
            MoveFixedTrait(command   = '',
                           key       = offsetCmd,
                           dx        = 15,
                           dy        = 15,
                           stack     = False),
            MoveFixedTrait(command   = '',
                           key       = deoffsetCmd, 
                           dx        = -15,
                           dy        = -15,
                           stack     = False),
            # No stacking - well - no movement 
            NoStackTrait(select      = NoStackTrait.NORMAL_SELECT,
                         bandSelect  = NoStackTrait.NEVER_BAND_SELECT,
                         move        = NoStackTrait.NEVER_MOVE,
                         canStack    = True,
                         ignoreGrid  = True,
                         description = 'Restrict movement of supplies'),
            # Show maximum attack support
            LabelTrait(
                label          = f'{{Maximum>0?"+":""}}',
                vertical       = LabelTraitCodes.BOTTOM,
                verticalJust   = LabelTraitCodes.BOTTOM,
                horizontal     = LabelTraitCodes.LEFT,
                horizontalJust = LabelTraitCodes.RIGHT,
                horizontalOff  = 5,                
                background     = rgb(255,255,255),
                foreground     = rgb(32,32,32),
                fontSize       = 20,
                fontStyle      = LabelTraitCodes.BOLD,
                nameFormat     = '$pieceName$',
                menuCommand    = '',
                propertyName   = 'Maximal'),
            CalculatedTrait(
                name       = 'IsAttacker',
                expression = f'{{Phase.contains("{faction}")}}'),
            TriggerTrait(
                command    = '',
                key        = combatSupplyReset,
                actionKeys = [eliminateCmd+'Enter'],
                property   = '{Maximum>0}'),
            ReportTrait(
                combatSupplyReset,
                report = (f'{{{verbose}?("!"+PrettyName+'
                          f'" sacrificed, move to OOB"):""}}')),
            TriggerTrait(
                command    = 'Sacrifice',
                key        = sacrificeKey,
                actionKeys = [key(NONE,0)+',toggleSacrifice'],
                property   = f'{{{limSac}Phase=="{vol[0]}"&&Moved!=true}}'),
            DynamicPropertyTrait(
                ['',key(NONE,0)+',toggleSacrifice',
                 DynamicPropertyTrait.DIRECT, '{Maximum>0?0:1}'],
                ['',key(NONE,0)+',resetSacrifice',
                 DynamicPropertyTrait.DIRECT, '{0}'],
                name = 'Maximum',
                numeric = True,
                value   = 0,
                description = 'Set support for maximum attack'),
            # Set or reset destination hex of replaced other supply 
            GlobalPropertyTrait(
                ['',eliminateCmd+'SetHex',GlobalPropertyTrait.DIRECT,
                 '{BasicName}'],
                ['',eliminateCmd+'ResetHex',GlobalPropertyTrait.DIRECT,''],
                name = supplyHex,
                description = 'Set the supply hex'),
            # Set supply taken or not taken
            GlobalPropertyTrait(
                ['',eliminateCmd+'SetHex',GlobalPropertyTrait.DIRECT,'{0}'],
                ['',eliminateCmd+'ResetHex',GlobalPropertyTrait.DIRECT,'{1}'],
                name = supplyTaken,
                description = 'Set the supply hex'),
            # Send the global hot key 
            GlobalHotkeyTrait(
                name         = '',
                key          = eliminateCmd+'OtherReplace',
                globalHotkey = newSupply+other,
                description  = 'Replace with other faction supply'),
            # Reinforce with this supply if no supply taken 
            TriggerTrait(
                command    = '', # 'Do reinforce',
                key        = reinforceKey+'Supply',
                actionKeys = [reinforceKey+'ReinforceSend',
                              eliminateCmd+'ResetHex'],
                property   = (f'{{{supplyTaken}<=0&&'
                              f'{supplyHex}==""&&'
                              f'Phase=="{reinf}"{extra}}}')),
            # Move in as replacement of opponents supply unit  
            SendtoTrait(name        = '',#'Replace send',
                        key         = reinforceKey+'CaptureSend',
                        mapName     = board, 
                        boardName   = board,
                        restoreKey  = '',
                        restoreName = '', 
                        destination = SendtoTrait.COUNTER,
                        expression  = f'{{BasicName=={supplyHex}}}', 
                        position    = '',
                        description = f'Replace as loc of {supplyHex}'),
            # Send eliminate to supply piece to replace 
            # GlobalCommandTrait(
            #     commandName = '',
            #     key         = eliminateCmd+'Captured',
            #     globalKey   = eliminateKey,
            #     ranged      = True,
            #     range       = 1,
            #     fixedRange  = True,
            #     properties  = f'{{BasicName=={supplyHex}}}'),
            # Move in supply as a replacement of captured enemy supply
            TriggerTrait(
                command    = '', #'Replace reinforce',
                key        = reinforceKey+'Supply',
                actionKeys = [reinforceKey+'CaptureSend',
                              eliminateCmd+'ResetHex'
                              # eliminateCmd+'Captured',
                              # eliminateCmd+'ResetHex'
                              ],
                property   = (f'{{{supplyTaken}<=0&&'
                              f'{supplyHex}!=""}}')),
            # Actually send the supply to destination - may be hex 
            SendtoTrait(name        = '',#'Actual send',
                        key         = reinforceKey+'ReinforceSend',
                        mapName     = board, 
                        boardName   = board,
                        restoreKey  = '',
                        restoreName = '', 
                        destination = SendtoTrait.GRID,
                        position    = f'{destName}',
                        description = f'Reinforce at {destName}'),
            # Eliminate voluntarialy
            TriggerTrait(
                command    = 'Eliminate',
                key        = eliminateKey,
                actionKeys = [eliminateCmd+'Enter'],
                property   = (f'{{(({volPhases})&&(Turn>1||'
                              f'Faction!="Allied"))||'
                              f'(({capPhases})&&{autoResults}!=true)}}')),
            # Move the supply back to OOB, first reset offset
            TriggerTrait(
                command    = '',
                key        = eliminateCmd+'Enter',
                actionKeys = [resetOffset,
                              key(NONE,0)+',resetSacrifice',
                              unloadCmd+'Real',
                              eliminateCmd+'Real',
                              eliminateCmd+'ResetHex']),
            # Move the supply unit back to the OOB
            SendtoTrait(name        = '',
                        key         = eliminateCmd+'Real',
                        restoreKey  = '',
                        restoreName = '', 
                        mapName     = f'{faction} OOB', 
                        boardName   = f'{faction} OOB',
                        destination = SendtoTrait.REGION,
                        region      = f'{{BasicName+"@{faction} OOB"}}',
                        description = f'Send to OOB {faction} OOB'),
            # Roll a dice 
            PrototypeTrait(name='Dice prototype'),
            # Check for capture or elimination 
            TriggerTrait(
                command    = 'Capture or eliminate',
                key        = captureKey,
                actionKeys = [unloadCmd+'Real',
                              dieRoll,
                              eliminateCmd+'MaybeCapture'],
                property   = f'{{({capPhases})&&{autoResults}==true}}'),
            TriggerTrait(
                command    = 'Capture',
                key        = captureUserKey,
                actionKeys = [unloadCmd+'Real',
                              eliminateCmd+'SetHex',
                              eliminateCmd+'OtherReplace',
                              eliminateCmd+'Enter'],
                property   = f'{{({capPhases})&&{autoResults}!=true}}'),
            # Replace with other if die roll less than 3. This first
            # sets the `supplyHex` to the basic name of the supply
            # piece to replace.  Then, it sends to global hot key to
            # move in the enemy supply piece.  This is done by the key
            # `reinforceKey+CaptureSend`, which is a trigger trait.
            # That trait then moves the new opponent piece in, and
            # then sends the eliminate command to this piece (via
            # stored name of the piece).  The reason we do it like
            # this, is because we cannot use `ReplacePiece`, since we
            # do not know which piece to get from the opponents OOB.
            #
            # There seems to be a sync issue here, in that sometimes,
            # the piece to be replaced is moved to the OOB _before_
            # the new enemy piece is moved in.
            TriggerTrait(
                command    = '', # 'Do replace',
                key        = eliminateCmd+'MaybeCapture',
                actionKeys = [eliminateCmd+'SetHex',
                              eliminateCmd+'OtherReplace',
                              eliminateCmd+'Enter'],
                property   = '{(Turn==1&&Faction=="Allied")?(Die<=4):(Die<=2)}'
            ),
            # If die roll said eliminate, not capture 
            TriggerTrait(
                command    = '',
                key        = eliminateCmd+'MaybeCapture',
                actionKeys = [eliminateCmd+'Enter'],
                property   = '{(Turn==1&&Faction=="Allied")?(Die>4):(Die>2)}'),
            #
            SubMenuTrait(
                subMenu = 'Operations',
                keys    = ['Sacrifice',
                           'Eliminate',
                           'Capture or eliminate',
                           'Capture']),
            # Reports
            # ReportTrait(
            #     eliminateCmd+'OtherReplace',
            #     report = (f'{{{verbose}?("~"+BasicName+" ask other {other} "'
            #               f'+"to replace supply at "+LocationName):""}}')),
            ReportTrait(
                eliminateCmd+'Captured',
                report = (f'{{{verbose}?("~"+BasicName+" eliminating "+'
                          f'{supplyHex}+" after moving to "+'
                          f'LocationName):""}}')),
            ReportTrait(
                reinforceKey+'ReinforceSend',
                report = (f'{{{verbose}?("!"+PrettyName+" appear at  "+'
                          f'"{destName} "+LocationName):""}}')),
            ReportTrait(
                reinforceKey+'CaptureSend',
                report = (f'{{{verbose}?("!"+PrettyName+" replace captured "+'
                          f'{supplyHex}+" at "+LocationName):""}}')),
            ReportTrait(
                eliminateCmd+'MaybeCapture',
                report = (f'{{{debug}?("~"+BasicName+" maybe capture ."+'
                          f'{supplyHex}+". and ."+{supplyTaken}+". "+'
                          f'Die):""}}')),
            ReportTrait(
                eliminateKey,
                report = (f'{{{verbose}?("!"+PrettyName+'
                          f'" send back to {faction} OOB at "+'
                          f'BasicName+"@{faction} OOB"):""}}')),
            ReportTrait(
                eliminateCmd+'SetHex',
                report = (f'{{{debug}?("~"+PieceName+'
                          f'" set hex ."+{supplyHex}+". and ."+'
                          f'{supplyTaken}+"."):""}}')),
            ReportTrait(
                setOffset,
                report = (f'{{{debug}?("~"+PieceName+'
                          f'"Set off set "+WasOffset):""}}')),
            ReportTrait(
                offsetCmd,
                report = (f'{{{debug}?("~"+PieceName+'
                          f'"Set moving piece a bit "+WasOffset):""}}')),
            # ReportTrait(
            #     reinforceKey+'Supply',
            #     report = (f'{{{verbose}?("!"+PieceName+'
            #               f'" reinforce at {destName} or hex"):""}}')),
            ReportTrait(
                loadCmd+'Real',
                report = (f'{{{verbose}?("!"+CurrentMatPieceName+" ("+'
                          f'CurrentMatID+") loads "+PieceName):""}}')),
            ReportTrait(
                unloadCmd+'Real',
                report = (f'{{{verbose}?("!"+OldMatPieceName+" ("+'
                          f'OldMatID+") unloads "+PieceName+" at "+'
                          f'LocationName):""}}'))
            #RestrictAccessTrait(sides = [],
            #                    description='No movement'),
        ]
        if faction == 'Allied':
            traits.extend([
                PrototypeTrait('C2 prototype'),
                CalculatedTrait(
                    name = 'Flipped',
                    expression = '{C2Status==2}')
            ])
            traits.insert(
                0,
                RestrictCommandsTrait(
                    name          = 'Restrict Ctrl-L to combat',
                    hideOrDisable = RestrictCommandsTrait.DISABLE,
                    expression    = (f'{{C2Status==2}}'),
                    keys          = [loadCmd+'Real']))
        else:
            traits.append(MarkTrait(name='Flipped',value='false'))
            
        traits.append(BasicTrait())
        prototypeContainer.addPrototype(
            name        = f'{faction} supply prototype',
            description = f'{faction} supply prototype',
            traits      = traits)
        
    # ----------------------------------------------------------------
    # Reinforcement option prototype
    # ReinforceOpt{opt}
    optReinfKey = key(NONE,0)+',optReinf'
    for faction,phase in zip(['Axis','Allied'],
                             [axReinf, alReinf]):
        traits = [
            ClickTrait(context = True),
            MarkTrait(name = 'Faction', value=faction),
            MarkTrait(name = 'Type',    value='ReinforceOption'),
            TriggerTrait(
                name       = 'Enable this reinforcement option',
                command    = 'Enable',
                key        = eliminateKey,
                actionKeys = [optReinfKey+'Select',
                              optReinfKey+'Disable',
                              optReinfKey+'Global'],
                property   = (f'{{Status>=0&&'
                              f'Phase=="{phase}"&&'
                              f'TurnOK==true}}')),
            GlobalHotkeyTrait(
                name         = '',
                key          = optReinfKey+'Global',
                globalHotkey = reinforceKey+faction,
                description  = 'Send global hot key'),
            LabelTrait(
                label        = f'{{Status<0?"-":""}}',
                vertical     = LabelTraitCodes.BOTTOM,
                verticalJust = LabelTraitCodes.BOTTOM,
                horizontal   = LabelTraitCodes.RIGHT,
                horizontalJust = LabelTraitCodes.RIGHT,
                background   = rgb(204,32,32),
                foreground   = rgb(255,255,255),
                fontSize     = 40,
                fontStyle    = LabelTraitCodes.BOLD,
                nameFormat   = '$pieceName$',
                menuCommand  = '',
                propertyName = 'Disable'),
            BasicTrait()]
        prototypeContainer.addPrototype(
            name        = f'{faction} reinforce prototype',
            description = f'{faction} reinforce prototype',
            traits      = traits)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    oddsP          = prototypes['OddsMarkers prototype']
    traits         = oddsP.getTraits()
    basic          = traits.pop()
    die            = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='Die')
    roll           = Trait.findTrait(traits,GlobalHotkeyTrait.ID,
                                     key='globalHotkey',
                                     value=key('6',ALT))
    res            = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='BattleResult')
    trg            = Trait.findTrait(traits,TriggerTrait.ID,
                                     key='key',value=resolveKey)
    rep            = None

    for t in traits:
        if t.ID != ReportTrait.ID: continue
        if not t['report'].startswith('{"` Battle # '): continue
        rep = t
        break

    getRoll              = f'GetString("{diceName}_result")'
    parseInt             = 'Integer.parseInt'
    die['expression'] = (f'{{{parseInt}({getRoll})}}')
    # res['expression'] = (f'{{({reg})}}')
    # Remove as we will add piece specific trait
    traits.remove(res)
    traits.insert(0,
                  RestrictCommandsTrait(
                      name          = 'Restrict Ctrl-Y to combat',
                      hideOrDisable = RestrictCommandsTrait.DISABLE,
                      expression    = (f'{{{globalResolve}==true}}'),
                      keys          = [resolveKey]))
    traits.insert(0,
                  RestrictCommandsTrait(
                      name          = 'Restrict assign result to combat',
                      hideOrDisable = RestrictCommandsTrait.DISABLE,
                      expression    = (f'{{{globalResolve}==true}}'),
                      keys          = [key(NONE,0)+f',wgMarkResult{d}real'
                                       for d in range(1,7)]))

    traits.extend([
        MarkTrait(name = pieceLayer, value = oddLayer),            
        basic])
    oddsP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Modify odds prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify results prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    traits.extend([
        RestrictCommandsTrait(
            name = 'Restrict counter attack',
            hideOrDisable = RestrictCommandsTrait.DISABLE, 
            expression    = (f'{{{globalResolve}==true}}'),
            keys          = counterKey),
        TriggerTrait(
            name       = 'Declare counter attack',
            command    = 'Counter-attack',
            key        = counterKey,
            actionKeys = [counterCmd+'Set',
                          setBattleKey,
                          resetPlacedKey,
                          counterCmd+'Start',
                          counterCmd+'Reset',
                          clearBattleKey,
                          ],
            #property   = f'{{{fromCounter}!=true}}'
        ),
        GlobalPropertyTrait(
            ['',counterCmd+'Set',GlobalPropertyTrait.DIRECT,'{true}'],
            ['',counterCmd+'Reset',GlobalPropertyTrait.DIRECT,'{false}'],
            name = counterAttack,
            description = 'Set for counter attack'),
        GlobalPropertyTrait(
            ['',counterCmd+'Set',GlobalPropertyTrait.DIRECT,'{true}'],
            name    = fromCounter,
        ),
        GlobalHotkeyTrait(name         = '',
                          key          = counterCmd+'Start',
                          globalHotkey = calcOddsKey+'ReAuto',
                          description  = 'Trampoline to global'),
        ReportTrait(
            counterCmd+'Start',
            report = (f'{{{debug}?("~"+BasicName+'
                      f'" calculate counter "+{counterAttack}):""}}')),
        ReportTrait(
            counterCmd+'Set',
            report = (f'{{{debug}?("~"+BasicName+" set counter attack "+'
                      f'{counterAttack}+" "+{fromCounter}):""}}')),
        ReportTrait(
            counterCmd+'Reset',
            report = (f'{{{debug}?("~"+BasicName+" reset counter attack "+'
                      f'{counterAttack}):""}}'))
    ])
        
    resP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Create die roller prototype
    traits = [
        CalculatedTrait(
            name = 'Die',
            expression = f'{{GetProperty("{diceName}_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = dieRoll,
            globalHotkey = diceKey,
            description  = 'Roll dice'),
        ReportTrait(dieRoll,
                    report = (f'{{{debug}?("~"+BasicName+'
                              f'" Rolled dice {diceName}: "+Die):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dice prototype',
                                    description = f'Dice prototype',
                                    traits      = traits)

                    
    # ----------------------------------------------------------------
    # Create prototype to update defender hexes (at most 6)
    traits = [
        CalculatedTrait(
            name        = 'InRough',
            expression  = (f'{{{globalRough}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in rough hex?'),
        CalculatedTrait(
            name        = 'InSwamp',
            expression  = (f'{{({globalSwamp}.contains(":"+'
                           f'LocationName+":"))}}'),
            description = 'Defender in swamp hex?'),
        CalculatedTrait(
            name        = 'InFort',
            expression  = (f'{{{globalFort}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in fort hex?'),
        CalculatedTrait(
            name        = 'InTobruk',
            expression  = (f'{{LocationName=="19@25"}}'),
            description = 'Defender in fort hex?'),
        CalculatedTrait(
            name        = 'InBardia',
            expression  = (f'{{LocationName=="17@30"}}'),
            description = 'Defender in fort hex?'),
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = [f'{updateHex}{no}Defender' for no in range(1,7)],
            property   = '{!IsAttacker}'),
        TriggerTrait(
            name       = '',
            key        = updateHex,
            #actionKeys = [],
            actionKeys = [f'{updateHex}Attacker'],
            property   = '{IsAttacker}'),
        ReportTrait(
            updateHex,
            report = (f'{{{debug}?("~"+BasicName+" "+LocationName'
                      f'+" In rough: "+InRough'
                      f'+" In Swamp: "+InSwamp'
                      f'+" In Fort: "+InFort'
                      f'+" Over escarpment: "+{overEscarpment}'
                      f'+" Defenders: "+{repDef}'
                      f'+" Escarpments: "+{repEdg}'
                      f'+""):""}}'))
    ]
    defhex    = []
    defrep    = []
    attedg    = []
    for no in range(1,7):
        notthis = '&&'.join([f'({defenderHex}{oth}!=LocationName)'
                             for oth in range(1,7) if oth != no])
        thisEmpty = f'{defenderHex}{no}==""'
        defhex.append(
            GlobalPropertyTrait(
                ['',f'{updateHex}{no}Defender',GlobalPropertyTrait.DIRECT,
                 f'{{(({thisEmpty}&&{notthis})?LocationName:'
                 f'{defenderHex}{no})}}'],
                name        = f'{defenderHex}{no}',
                numeric     = True,
                description = 'Update defender hex to this unit'))
        defrep.append(
            ReportTrait(
                f'{updateHex}{no}Defender',
                report = (f'{{{debug}?("~"+PrettyName+" @ "+LocationName+'
                          f'" update defender hex {no}: "+{repDef}):""}}')))
        attedg.append(
            CalculatedTrait(
                name       = f'{overEscarpment}{no}',
                expression = (f'{{({defenderHex}{no}=="")?0:'
                              f'({globalEscarpments}.contains('
                              f'{defenderHex}{no}+LocationName)?1:-1)'
                              f'}}')))
    sumOver = '+'.join([f'{overEscarpment}{no}' for no in range(1,7)])
    absOver = '+'.join([f'Math.abs({overEscarpment}{no})' for no in range(1,7)])
    # Store the over-escarpment calculation 
    traits.append(
        DynamicPropertyTrait(
            ['',f'{updateHex}Attacker',DynamicPropertyTrait.DIRECT,
             f'{{({sumOver})==({absOver})}}'],
            name = f'{overEscarpment}',
            numeric = True,
            value = False,
            description = 'If attacking over escarpment'))
    traits.extend(defhex+defrep+attedg)
    traits.append(BasicTrait())
    prototypeContainer.addPrototype(name        = f'Defender hex prototype',
                                    description = f'Defender hex prototype',
                                    traits      = traits)


    # ----------------------------------------------------------------
    # Create terrain prototype
    board    = main['mapName']
    traits = [BasicTrait()]
    prototypeContainer.addPrototype(name        = f'Terrain prototype',
                                    description = f'Terrain prototype',
                                    traits      = traits)

    
    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    #
    # Need a bit more massage 
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effAF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveAF')
    effDF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveDF')
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    calAF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleAF)
    calDF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleDF)
    isAtt       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='IsAttacker')
    calAFCmds   = calAF.getCommands()
    calAFCmd    = calAFCmds[0]
    calAFkey    = calAFCmd[1]
    calAFCmd[1] = calAFkey+'Real'
    calDFCmds   = calDF.getCommands()
    calDFCmd    = calDFCmds[0]
    calDFkey    = calDFCmd[1]
    calDFCmd[1] = calDFkey+'Real'
    calAF.setCommands(calAFCmds)
    calDF.setCommands(calDFCmds)
    # print('Calculate AF commands',calAFCmds)
    # print('Calculate DF commands',calDFCmds)
    # print(oddsS.getCommands())
    if effAF: traits.remove(effAF)
    if effDF: traits.remove(effDF)
    # if oddsS: traits.remove(oddsS)

    isAttExr = isAtt['expression'][1:-1]
    isAtt['expression'] = (f'{{({counterAttack}==true)?'
                           f'(!{isAttExr}):({isAttExr})}}')
    
    toremove = []
    for trait in traits:
        if trait.ID != ReportTrait.ID: continue
        # print(trait['report'])
        if 'to total attack factor' in trait['report']:
            toremove.append(trait)
        if 'to total defence factor' in trait['report']:
            toremove.append(trait)
    for trait in toremove:
        # print('Remove',trait['report'])
        traits.remove(trait)


    traits.extend([
        CalculatedTrait(
            name        = 'EffectiveAF',
            expression  = ('{(Flipped==true)?0:'
                           'Math.max(FactorAF*CurrentCF,1)}'),
            description = 'Calculate effective CF'),
        CalculatedTrait(
            name        = 'EffectiveDF',
            expression  = ('{Math.max(FactorDF*CurrentCF,1)}'),
            description = 'Calculate effective CF'),
        CalculatedTrait(
            name        = 'FactorSupply',
            expression  = '{CombatSupply==1?0.5:(CombatSupply==3?2:1)}'),
        # In counter attacks, always use face-value 
        CalculatedTrait(
            name        = 'FactorAF',
            expression  = (f'{{{counterAttack}!=true?'
                           f'(({overEscarpment}?0.5:1)*FactorSupply):'
                           f'CounterFactor}}'),
            description = 'Attack factor multiplier'),
        # In counter attacks, always use face-value - no terrain
        CalculatedTrait(
            name        = 'FactorDF',
            expression  = (f'{{({counterAttack}==true)?1:('
                           f'(InRough?2:1)*'
                           f'((Faction=="Axis"&&InSwamp==true)?2:1)*'
                           f'(InBardia?2:1)*'
                           f'(InTobruk==true?5:1))}}'),
            description = 'Terrain multiplifier'),
        CalculatedTrait(
            name        = 'ColumnNumber',
            expression  = ('{CurrentZone=="hexes"?'
                           '(Integer.parseInt(LocationName.substring(0,2))):'
                           '-1}')),
        CalculatedTrait(
            name        = 'RowNumber',
            expression  = ('{CurrentZone=="hexes"?'
                           '(Integer.parseInt(LocationName.substring(3,5))):'
                           '-1}')),
        CalculatedTrait(
            name        = 'InLibya',
            expression  = ('{CurrentZone=="hexes"&&('
                           '(RowNumber<=28&&ColumnNumber!=6)||'
                           '(RowNumber==29&&'
                           '(ColumnNumber==1||ColumnNumber==3||'
                           '(ColumnNumber>=9&&ColumnNumber!=14)))||'
                           '(RowNumber==30&&'
                           '(ColumnNumber==11||ColumnNumber>=15)))}')),
        PrototypeTrait(name='Defender hex prototype'),
        GlobalPropertyTrait(
            ['',calcDF,GlobalPropertyTrait.INCREMENT,'{CurrentCF}'],
            name = bareDF,
            description = 'Update bare DF'),
        ReportTrait(
            calcDF,
            report = (f'{{{verbose}?("!"+BasicName+" update bare CF "+'
                      f'CurrentCF+" -> "+{bareDF}):""}}')),
        TriggerTrait(
            name       = '',
            key        = calAFkey,
            actionKeys = [calAFCmd[1]]),
        TriggerTrait(
            name       = '',
            key        = calDFkey,
            actionKeys = [calDFCmd[1]]),
        TriggerTrait(
            name       = 'Print',
            command    = 'Print',
            key        = printKey,
            property   = f'{{{verbose}}}',
            actionKeys = []),
        ReportTrait(printKey,
                    report=(f'{{BasicName+":"'
                            f'+" "+PrettyName'
                            f'+" ("+Type+" and "+MainType+")"'
                            f'+" Nation="+Nation'
                            f'+" Coords="+ColumnNumber+","+RowNumber'
                            f'+" InLibya="+InLibya'
                            f'+" Faction="+Faction'
                            f'+" Hex="+LocationName+" ("'
                            f'+" Rough="+InRough'
                            f'+" Swamp="+InSwamp'
                            f'+" Fort="+InFort+")"'
                            f'+" FactorDF="+FactorDF'
                            f'+" FactorAF="+FactorAF'
                            f'+" AF="+EffectiveAF'
                            f'+" DF="+EffectiveDF'
                            f'+" CF="+CurrentCF'
                            f'+" CounterFactor="+CounterFactor'
                            f'+" OverEscarpment="+{overEscarpment}'
                            f'+" Attacker="+IsAttacker'
                            f'+" Step="+Step_Level'
                            f'+" Supply="+Supply_Level'
                            f'+" Mount="+Dismount_Level'
                            f'+" C2="+C2Status'
                            f'+" Augment="+AugmentStatus'
                            f'+" Flipped="+Flipped'
                            f'+" SU0="+SU0'
                            f'+" SU1="+SU1'
                            f'}}')),
        ReportTrait(calAFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName'
                              f'+" Hex="+LocationName+" ("'
                              f'+" Rough="+InRough'
                              f'+" Swamp="+InSwamp'
                              f'+" Fort="+InFort+")"'
                              f'+" AF="+CF+" "+EffectiveAF+"=> "'
                              f'+{battleAF}'
                              f'):""}}')),
        ReportTrait(calDFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName'
                              f'+" Hex="+LocationName+" ("'
                              f'+" Rough="+InRough'
                              f'+" Swamp="+InSwamp'
                              f'+" Fort="+InFort+")"'
                              f'+" DF="+DF+" "+EffectiveDF+"=> "'
                              f'+{battleDF}'
                              f'):""}}')),
        # ReportTrait(updateHex,
        #             report = (f'{{{debug}?("! "+BasicName+" in "'
        #                       f'+LocationName+" update hex -> "'
        #                       f'+" Attacker: "+IsAttacker'
        #                       f'+" In rough: "+InRough'
        #                       f'+" In Swamp: "+InSwamp'
        #                       f'+" In Fort: "+InFort'
        #                       f'):""}}')),
        basic
        ])
    rest = RestrictCommandsTrait(
        keys          = [declareKey],
        name          = 'Disable when not in combat',
        hideOrDisable = RestrictCommandsTrait.DISABLE,
        expression    = f'{{{globalDeclare}==true}}')
    battleUnitP.setTraits(rest,*traits)

    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    #
    #
    #
    limitAF     = key(NONE,0)+',limitAF'
    limitDF     = key(NONE,0)+',limitDF'
    addRough    = key(NONE,0)+',addRough'
    addSwamp    = key(NONE,0)+',addSwamp'
    addFort     = key(NONE,0)+',addFort'
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcOdds    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcOddsKey)
    calcFrac    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcFracKey)
    batFrac    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'BattleFraction')
    oddsIdx    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'OddsIndex')
    oddsIdxLim = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'OddsIndexLimited')
    oddsIdxRaw = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'OddsIndexRaw')
    oldFrac    = batFrac['expression'][1:-1]
    oldRaw     = oddsIdxRaw["expression"]
    oldIx      = oddsIdx['expression'][1:-1]
    oldLim     = oddsIdxLim['expression']
    # Counter attacks can be done at negative CF difference
    minLoss    = {k: min(v) for k,v in crt.items()}
    minRes     = '||'.join([f'(OddsIndexRaw=={k+1}&&{m}>={bareDF})'
                            for k,m in minLoss.items()])
    zeroExpr   = f':({counterAttack}==true?1:0)'
    zeroExpr   = f':({counterAttack}==true?2:1)'
    avExpr     = (f'(Phase.contains("movement")&&{counterAttack}!=true)?'
                  f'(({minRes})?13:0):OddsIndexRaw')
    # print('Old limited:', oddsIdxLim['expression'])
    # print('Old Raw:    ', oldRaw)
    #.replace(':0',zeroExpr)\
    newRaw     = oldRaw\
        .replace(f':{battleFrac}>=-1?(1):0', zeroExpr)\
        .replace(f'{battleFrac}>=11?(12):','')
    newLim     = f'{avExpr}'
    # print('New Raw:    ',newRaw)
    # print('New limited:',newLim)
    oddsIdxRaw['expression'] = f'{newRaw}'
    oddsIdxLim['expression'] = f'{{{newLim}}}'

    rep = None
    for t in traits:
        if t.ID == ReportTrait.ID and 'Battle #' in t['report']:
            rep = t
            break

    traits.extend([
        GlobalPropertyTrait(
            ['',zeroBattleDF,GlobalPropertyTrait.DIRECT,'{0}'],
            name = bareDF,
            description = 'Zero bare DF'),
        ReportTrait(
            zeroBattleDF,
            report = (f'{{{debug}?("~"+BasicName+" zero bare DF "+'
                      f'{bareDF}):""}}'))])
    resetHex = [
        GlobalPropertyTrait(
            ['',updateHex+f'Reset{no}',GlobalPropertyTrait.DIRECT,
             f'{{""}}'],
            name        = f'{defenderHex}{no}',
            numeric     = False,
            description = 'Reset defender rough hex')
        for no in range(1,7)]
        
    resetHexes = [updateHex+f'Reset{no}' for no in range(1,7)]
    traits.extend(resetHex+
                  [ReportTrait(
                      *resetHexes,
                      report=(f'{{{debug}?("~ Reset defender hexes:"+'
                              f'{repDef}):""}}'))])
    traits.extend([
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkDeclare,GlobalPropertyTrait.DIRECT,
             f'{{!('
             f'(Turn>1||!Phase.contains("Allied"))&&' # Not allied turn 1
             f'(Phase=="{alCombat}"||'
             f'Phase=="{alComSup}"||'
             f'Phase=="{alMove}"||'
             f'Phase=="{axCombat}"||'
             f'Phase=="{axComSup}"||'
             f'Phase=="{axMove}"))}}'],
            name = globalDeclare,
            numeric = True,
            description = 'Check for (any) declare phase'),
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkResolve,GlobalPropertyTrait.DIRECT,
             f'{{!((Turn>1||!Phase.contains("Allied"))' # Not allied turn 1
             f'&&(Phase.contains("combat")||'
             f'Phase.contains("movement")))}}'],
            name        = globalResolve,
            numeric     = True,
            description = 'Check for (any) resolve phase'),
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkMaltaKey,GlobalPropertyTrait.DIRECT,
             f'{{!(Phase=="{axSupply}"&&Turn==15&&{maltaStatus}==0)}}'],
            name        = maltaNotPossible,
            numeric     = True,
            description = 'Check if Malta can be invaded'),
        ReportTrait(
            checkMaltaKey,
            report = (f'{{{verbose}?({maltaNotPossible}==true?'
                      f'"`Axis Malta invasion not possible":'
                      f'"!Axis Malta invasion is possible"):""}}')),
        # --- Set not setup phase -----------------------------------
        GlobalPropertyTrait(
            ['',checkSetup,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="Setup"||Turn!=1}}'],
            name        = globalSetup,
            numeric     = True,
            description = 'Check for setup phase'),
        # --- Clear supply taken and supply hex ----------------------
        # Note, we re-use a command 
        GlobalPropertyTrait(
            ['',checkDeclare,GlobalPropertyTrait.DIRECT,'{0}'],
            name = supplyTaken,
            numeric = True,
            description = 'Clear supply taken'),
        GlobalPropertyTrait(
            ['',checkDeclare,GlobalPropertyTrait.DIRECT,''],
            name = supplyHex,
            description = 'Clear supply taken'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHex+'Real',
            globalHotkey = updateHex,
            description  = 'Ask GKC to update defender hex'),
        # --- Update defender hexes -----------------------------------
        TriggerTrait(
            name       = '',
            key        = updateHex,
            actionKeys = resetHexes+[updateHex+'Real']),
        # --- Reports ------------------------------------------------
        ReportTrait(checkDeclare,
                    report = (f'{{{debug}?("~ Check for declare phase "+'
                              f'Phase+" -> {globalDeclare}="+'
                              f'{globalDeclare}):""}}')),
        ReportTrait(checkResolve,
                    report = (f'{{{debug}?("~ Check for resolve phase "+'
                              f'Phase+" -> {globalResolve}-"+'
                              f'{globalResolve}):""}}')),
        ReportTrait(checkSetup,
                    report = (f'{{{debug}?("~ Check for setup phase "+'
                              f'Phase+" -> {globalSetup}-"+'
                              f'{globalSetup}):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("~ "+BasicName+" Update def hex"+'
                              f'{repDef}):""}}')),
        ReportTrait(calcIdxKey,
                    report = (f'{{{debug}?("~ "+BasicName+'
                              f'" Index calculation:"+'
                              f'" Total DF="+{battleDF}+'
                              f'" Total AF="+{battleAF}+'
                              f'" Fraction="+{battleFrac}+'
                              f'""'
                              f'):""}}')),
            
        basic
    ])
    keys = calcFrac.getActionKeys()
    calcFrac.setActionKeys(keys[:-1]+
                           #[addRough,addSwamp,addFort]+
                           [limitAF,limitDF]+keys[-1:])
    keys = calcOdds.getActionKeys()
    calcOdds.setActionKeys([updateHex]+keys)
    battleCalcP.setTraits(*traits)
        
    # ----------------------------------------------------------------
    # Prototype for retire withdraw
    icnt = [0]
    acnt = [0]
    ccnt = [0]
    jcnt = [0]
    turns = {2:  ['i', 'i'],
             5:  ['i cadre'],
             6:  ['i', 'i cadre'],
             9:  ['a'],
             10: ['a'],
             14: ['i in', 'i in'],
             19: ['a']}
    traits = []
    keyw   = key(NONE,0)+',withdraw'
    allc   = []
    # Turn number and what to withdraw 
    for no, what in turns.items():
        sc = []
        # Loop over what to withdraw
        for w in what:
            ww     = w[0]           # Type 
            cadre  = 'cadre' in w   # From cadre 
            ind    = 'in' in w      # Indian? 
            tpe    = 'Infantry' if ww == 'i' else 'Armoured'
            # Determine the counter 
            cnt    = (ccnt if ww == 'i' and cadre else
                      (jcnt if ind else
                       (icnt if ww == 'i' else acnt)))
            # Build target location 
            tgt    = (('in ' if ind else 'br ')
                      + ww
                      + ' withdraw'
                      + (' cadre' if cadre else '')
                      + '@Allied OOB'+(f'_{cnt[0]}' if cnt[0] > 0 else ''))
            # Build the variable
            shrt   = tgt.replace(' ','_').replace('@','_')
            keyc   = key(NONE,0)+','+shrt
            # Build the condition
            cond   = (f'Turn=={no}&&'
                      f'CurrentZone=="hexes"&&'
                      f'MainType=="{tpe}"'
                      f'&&{shrt}==0')
            # print(shrt)
            cnt[0] += 1
            if cadre:
                # Also allow from cadre 
                cond = cond.replace(
                    'CurrentZone=="hexes"',
                    '(CurrentZone=="hexes"||CurrentZone=="Allied cadre")')
            if ind:
                # Specifically indian units 
                cond += '&&BasicName.startsWith("in ")'
            #print(f' Adding sendto with {tgt} and trigger with {cond} and {shrt}')
            sc.append(cond)
            traits.extend([
                # The trait that sends to the destination 
                SendtoTrait(
                    mapName        = 'Allied OOB',
                    boardName      = 'Allied OOB',
                    destination    = SendtoTrait.REGION,
                    region         = tgt,
                    key            = keyc,
                    restoreKey     = '',
                    restoreName    = ''
                    
                ),
                # Trait to set variable 
                GlobalPropertyTrait(
                    ['',keyc+'Set',GlobalPropertyTrait.DIRECT,'{1}'],
                    ['',keyc+'Reset',GlobalPropertyTrait.DIRECT,'{0}'],
                    name = shrt,
                    numeric = True
                ),
                # Trigger withdrawal 
                TriggerTrait(
                    name       = '',
                    command    = '',
                    key        = keyw,
                    actionKeys = [keyc+'Set',resetStatus,keyc],
                    property   = f'{{{cond}}}'),
                ReportTrait(
                    keyc,
                    report = (f'{{{verbose}?("!"+PrettyName+'
                              f'" withdrawn to {tgt}"):""}}'))
            ])
        allc.append(f'({"||".join(sc)})')

    allc = (f'Phase=="{alReinf}"&&'
            f'({"||".join(allc)})')
    #print(allc)

    # Make the prototype with a master trigger trait.  Note that we
    # replicate most of the conditions here, so that the withdraw
    # command is grayed-out when not available.
    traits.extend([
        TriggerTrait(
            name       = '',
            command    = 'Withdraw',
            key        = withdrawKey,
            actionKeys = [keyw],
            property   = (f'{{{allc}}}')
        ),
        BasicTrait()])
    prototypeContainer.addPrototype(name        = f'Withdraw prototype',
                                    description = f'Withdraw prototype',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Reenter prototype
    traits = []
    for tkey, slot in reenter_slots.items():
        turn   = int(tkey)
        var    = slot.replace(' ','_')
        wslots = [f'LocationName=="{w.replace("_"," ")
                                     .replace(" A","@A")
                                     .replace(" 1","_1")
                                     .replace(" 2","_2")
                                     .replace(" 3","_3")}"'
                  for wt,w in withdraw_slots.items()
                  if wt < turn and w[3] == 'i']
        wslots = '||'.join(wslots)
        cond   = (f'{var}==0&&'
                  f'MainType=="Infantry"&&'
                  f'Turn=={turn}&&'
                  f'(CurrentZone=="Allied cadre"||'
                  f'(CurrentZone=="Allied OOB"&&'
                  f'({wslots})))')
        rkey   = key(NONE,0)+','+var
        traits.extend([
            GlobalPropertyTrait(
                ['',rkey+'SetTurn',GlobalPropertyTrait.DIRECT,
                 f'{{{turn}}}'],
                name = 'ReenterTurn',
                numeric = True),
            GlobalPropertyTrait(
                ['',rkey+'Set',GlobalPropertyTrait.DIRECT,f'{{1}}'],
                name = var,
                numeric = True),
            SendtoTrait(
                name           = '',
                key            = rkey+'Send',
                restoreKey     = '',
                restoreName    = '',
                mapName        = 'Allied OOB',
                boardName      = 'Allied OOB',
                destination    = SendtoTrait.REGION,
                region         = slot+'@Allied OOB'),
            # If conditions are true
            # (from cadre, or previous withdraw slot,
            # the turn type - Infantry - and right turn), then
            # - Set the taken property for this reenter slot
            # - Set the turn to reenter to global property
            # - Take global property as reinforce turn
            # - Send to the reenter slot 
            TriggerTrait(
                name           = 'Put in reenter slot',
                command        = '',
                key            = reenterKey,
                actionKeys     = [rkey+'Set',
                                  rkey+'SetTurn',
                                  reinforceKey+'Turn',
                                  rkey+'Send'
                                  ],
                property       = (f'{{{cond}}}')),
            ReportTrait(
                rkey+'Send',
                report = (f'{{{verbose}?("!"+PrettyName+'
                          f'" moved to reenter slot {slot}"):""}}'))
        ])

    traits.append(BasicTrait())
    prototypeContainer.addPrototype(name        = f'Reenter prototype',
                                    description = f'Reenter prototype',
                                    traits      = traits)
    

    # ----------------------------------------------------------------
    # Modify faction prototypes 
    alexandria = ['1350', '1351', '1352',
                  '1250', '1251', '1451']
    for faction in ['Allied', 'Axis']:
        factionp = prototypes[f'{faction} prototype']
        ftraits  = factionp.getTraits()
        fbasic   = ftraits.pop()
        destName = (hexName(alexandriaC) if faction == 'Allied' else
                    hexName(elAgheilaC))
        
        # Remove traits we don't want nor need
        # fdel     = Trait.findTrait(ftraits,DeleteTrait.ID)
        # fdel['key'] = deleteKey
        # fdel['name'] = ''

        felim        = Trait.findTrait(ftraits,SendtoTrait.ID,
                                       'key', eliminateKey)
        felim['key']         = eliminateCmd
        felim['name']        = ''
        felim['mapName']     = board
        felim['boardName']   = board
        felim['region']      = f'{faction} cadre'
        felim['destination'] = SendtoTrait.REGION

        diskey               = key(NONE,0)+',Dismount'
        mntkey               = key(NONE,0)+',Mount'
        supphase             = alSupply if faction == 'Allied' else axSupply
        reiphase             = alReinf  if faction == 'Allied' else axReinf
        
        ftraits.extend([
            # Not sure this is needed
            DeleteTrait(
                name = '',
                key  = deleteKey),
            SubMenuTrait(
                subMenu = 'Various',
                keys    = ['Movement Trail',
                           'Rotate CW',
                           'Rotate CCW',
                           'Mark moved',
                           'Restore',
                           'Print']),
            SubMenuTrait(
                subMenu = 'Combat',
                keys    = ['Declare battle',
                           'Eliminate']),
            MarkTrait(
                name       = pieceLayer,
                value      = unitLayer),
            PrototypeTrait(name = 'Sea prototype'),
            PrototypeTrait(name = f'{faction} OOS prototype'),
            TriggerTrait(
                name       = 'Eliminate unit',
                command    = 'Eliminate',
                key        = eliminateKey,
                actionKeys = [
                    resetStatus,
                    clearBattleKey,
                    eliminateCmd+'Step'
                ],
                property = (f'{{Phase.contains("combat")||'
                            f'Phase.contains("movement")}}')
            ),
            TriggerTrait(
                name = 'Eliminate unit',
                command = '',
                key     = eliminateCmd+'Step',
                actionKeys = [eliminateCmd+'Register',
                              eliminateCmd]),
            #ReportTrait(
            #    eliminateKey,
            #    report = (f'{{{debug}?("~Eliminate "+BasicName):""}}')),
            ReportTrait(
                eliminateCmd,
                report = (f'{{{verbose}?("!Eliminate "+PrettyName):""}}')),
            TriggerTrait(
                name       = 'Mark as dismounted',
                command    = 'Dismount',
                key        = dismountKey,
                actionKeys = [diskey],
                property   = (f'{{Dismount_Level==1&&'
                              f'CurrentZone=="hexes"&&'
                              f'Phase=="{reiphase}"}}')),
            TriggerTrait(
                name       = 'Mark as re-mounted',
                command    = 'Mount',
                key        = mountKey,
                actionKeys = [mntkey],
                property   = f'{{Dismount_Level==2&&Phase=="{reiphase}"}}'),
            SubMenuTrait(
                subMenu = 'Status',
                keys = ['Out-of-Supply',
                        'In-supply',
                        'Dismount',
                        'Mount',
                        'Out-of-C2',
                        'In C2'
                        ]),
            LayerTrait(['',
                        'grounded-mark.png'],
                       ['',
                        'Dismounted +'],
                       activateName = '',
                       activateMask = '',
                       activateChar = '',
                       increaseName = '',
                       increaseMask = '',
                       increaseChar = '',
                       decreaseName = '',
                       decreaseMask = '',
                       decreaseChar = '',
                       resetName    = '',
                       resetKey     = resetStatus,
                       under        = False,
                       underXoff    = 0,
                       underYoff    = 0,
                       loop         = True,
                       name         = 'Dismount',
                       description  = '(Un)Mark unit as dismounted',
                       always       = True,
                       activateKey  = '',
                       increaseKey  = diskey,
                       decreaseKey  = mntkey, #key('G',CTRL_SHIFT),
                       scale        = 1),
            SendtoTrait(name = '',
                        key         = reinforceKey+'Send',
                        mapName     = board,
                        boardName   = board,
                        destination = SendtoTrait.GRID,
                        position    = destName,
                        description = f'Reinforce at {destName}'),
            ReportTrait(
                reinforceKey+'Send',
                report = (f'{{{verbose}?("!"+PrettyName+'
                          f'" reinforce at {destName}"):""}}')),
            LabelTrait(
                label          = (fr'{{(CombatSupply==1?"1\/2x":'
                                  f'(CombatSupply==2?"":'
                                  f'(CombatSupply==3?"2x":"")))}}'),
                vertical       = LabelTraitCodes.BOTTOM,
                verticalJust   = LabelTraitCodes.BOTTOM,
                verticalOff    = 3,
                horizontal     = LabelTraitCodes.LEFT,
                horizontalJust = LabelTraitCodes.RIGHT,
                horizontalOff  = 8,
                background     = rgb(255,255,255),
                foreground     = rgb(32,32,32),
                fontSize       = 20,
                fontStyle      = LabelTraitCodes.BOLD,
                nameFormat     = '$pieceName$',
                menuCommand    = '',
                propertyName   = 'CombatSupplyText'),
            RestrictCommandsTrait(
                name          = 'Restrict Combat supplies to combat',
                hideOrDisable = RestrictCommandsTrait.DISABLE,
                expression    = (f'{{Phase!="{faction} combat supply"&&'
                                 f'Phase!="{faction} movement"}}'),
                keys          = [combatSupplyGeneral,
                                 combatSupplyAttack,
                                 combatSupplyMaximal]),
            DynamicPropertyTrait(
                ['General (<=20MF)', combatSupplyGeneral,
                 DynamicPropertyTrait.DIRECT, '{1}'],
                ['Attack (<=8MF)', combatSupplyAttack,
                 DynamicPropertyTrait.DIRECT, '{2}'],
                ['Maximal (<=8MF+Supply)', combatSupplyMaximal,
                 DynamicPropertyTrait.DIRECT, '{3}'],
                ['',combatSupplyReset,
                 DynamicPropertyTrait.DIRECT, '{2}'],
                name = 'CombatSupply',
                numeric = True,
                value = 2,
                description = 'Units compat supply status'),
            SubMenuTrait(subMenu = 'Combat supply',                        
                         keys    = ['General (<=20MF)',
                                    'Attack (<=8MF)',
                                    'Maximal (<=8MF+Supply)'])
                
        ])

        if faction == 'Allied':
            inAlexandria = '||'.join([f'LocationName=="{hexName(h)}"' for
                                      h in alexandria])
            ftraits.extend([
                PrototypeTrait(name = 'Withdraw prototype'),
                PrototypeTrait(name = 'Reenter prototype'),
                PrototypeTrait(name = 'C2 prototype'),
                CalculatedTrait(
                    name       = 'Flipped',
                    expression = ('{C2Status>1||'
                                  'Supply_Level>1||'
                                  'Dismount_Level>1}')),
                DynamicPropertyTrait(
                    ['',augStepKey,
                     DynamicPropertyTrait.DIRECT,
                     '{AugmentStatus+1}'],
                    ['',augRst, DynamicPropertyTrait.DIRECT, '{0}'],
                    ['',resetStatus, DynamicPropertyTrait.DIRECT, '{0}'],
                    name    = 'AugmentStatus',
                    numeric = True,
                    value   = 0),
                CalculatedTrait(
                    name       = 'InAlexandria',
                    expression = f'{{{inAlexandria}}}'),
                CalculatedTrait(
                    name       = 'CurrentCF',
                    expression = '{CF+AugmentStatus}'),
                SubMenuTrait(
                    subMenu = 'R&R',
                    keys = ['Withdraw',
                            'Reinforce',
                            'Replace'])
                    
                ])
        else:
            ftraits.extend([
                CalculatedTrait(
                    name       = 'Flipped',
                    expression = ('{Supply_Level>1||'
                                  'Dismount_Level>1}')),
                CalculatedTrait(name = 'CurrentCF',
                                expression= '{CF}'),
                MarkTrait(name='C2Status',value=1),
                SubMenuTrait(
                    subMenu = 'R&R',
                    keys = ['Replace'])
            ])
            


        # ftraits.remove(fdel)
        factionp.setTraits(*ftraits,fbasic)

    # ----------------------------------------------------------------
    # Prototype for Italian infantry
    traits = [
        PrototypeTrait(name = 'Dice prototype'),
        GlobalPropertyTrait(
            ['',eliminateCmd+'Register',
             GlobalPropertyTrait.INCREMENT, '{CF}'],
            name = itInfantryLoss,
            description = 'Increment count of Italian Infantry CF loss'),
        ReportTrait(
            eliminateCmd+'Register',
            report = (f'{{{verbose}?("~"+PrettyName+" eliminated ("+'
                      f'CF+") total Italian Infantry loss: "+'
                      f'{itInfantryLoss}):""}}')),
        CalculatedTrait(
            name       = 'SU1',
            expression = (
                r'{CountRange(0,1,'
                r'"{(MainType==\"Armoured\"&&Nation==\"Italian\")'
                r'||Nation==\"German\"}")}')),
        CalculatedTrait(
            name       = 'SU0',
            expression = (
                r'{CountLocation('
                r'"{(MainType==\"Armoured\"&&Nation==\"Italian\")'
                r'||Nation==\"German\"}")}')),
        # Only show if enabled 
        RestrictCommandsTrait(
            hideOrDisable = RestrictCommandsTrait.HIDE,
            expression    = f'{{{itInfantryOpt}!=true||{autoResults}!=true}}',
            keys          = [surrenderKey]),
        TriggerTrait(
            name       = '',
            command    = 'Surrender',
            key        = surrenderKey,
            actionKeys = [dieRoll,
                          eliminateCmd+'Surrender'],
            property   = (f'{{('
                          f'CurrentZone=="hexes"'
                          f'&&Phase=="{alCombat}"&&'
                          f'(({itInfantryLoss}>=20&&SU1<=0)||'
                          f'({itInfantryLoss}>=25&&SU0<=0)))}}')),
        TriggerTrait(
            name       = '',
            command    = '',
            key        = eliminateCmd+'Surrender',
            actionKeys = [eliminateCmd],
            property   = '{Die<=3}'),
        ReportTrait(
            surrenderKey,
            report = (f'{{{debug}?("~"+PrettyName+" may surrender"):""}}')),
        ReportTrait(
            dieRoll,
            report = (f'{{{debug}?("~"+PrettyName+" die roll: "+Die):""}}')),
        ReportTrait(
            eliminateCmd+'Surrender',
            report = (f'{{{verbose}?(Die<=3?("!"+PrettyName+'
                      f'" surrenders"):""):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(
        name        = f'Italian infantry prototype',
        description = f'Italian infantry prototype',
        traits      = traits)
    
            
    
    # ----------------------------------------------------------------
    #
    # Get all pieces and modify them
    #
    # Start with RP pieces 
    turnRPs = {
        'br i rp': {i:2 for i in range (1,21) },
        'br a rp': {i: 3 if i == 1 else 1 for i in range(1,21) },
        'de i rp': {i:1 for i in [5,10,11,13,15,17,18,19,20]},
        'de a rp': {i: 3 if i in [5,6] else (2 if i == 7 else 1)
                    for i in [1]+list(range(5,21))},
        'it i rp': {i:4 if 1 == i else (2 if i in [4,16] else 1)
                    for i in [1]+list(range(4,21))},
        'it a rp': {i:1 for i in [6,12,14,18,20]}
    }
        
        
    pieces      = game.getPieces(asdict=False)
    pieces_dict = game.getPieces(asdict=True)
    toRemove    = {}
    # from data import setups
    preToNation = {'br': 'Allied',
                   'in': 'Allied',
                   'pl': 'Allied',
                   'ff': 'Allied',
                   'gk': 'Allied',
                   'be': 'Allied',
                   'au': 'Allied',
                   'nz': 'Allied',
                   'sa': 'Allied',
                   'us': 'Allied',
                   'de': 'German',
                   'it': 'Italian'}
    preToFaction = {'br': 'Allied',
                    'de': 'Axis',
                    'it': 'Axis'}
    
    for rpName,turns in turnRPs.items():
        piece   = pieces_dict.get(rpName)
        traits  = piece.getTraits()
        basic   = traits.pop()
        faction = preToFaction[rpName[:2]]
        nation  = preToNation [rpName[:2]]
        track   = ('br' if faction == 'Allied' else 'de')+' rp'
        tpe     = 'Inf' if ' i rp' in rpName else 'Arm'
        var     = nation+tpe+'RP'
        prty    = f'{nation} {tpe} RPs'
        max     = (None if faction == 'Axis' else
                   globalMaxInf if tpe == 'Inf' else globalMaxArm)

        factP   = Trait.findTrait(traits, PrototypeTrait.ID,
                                  'name', f'{faction} prototype')
        if factP: traits.remove(factP)

        # print(f'{rpName} {nation} {faction} {var} {tpe} {max}')
        traits.extend([
            MarkTrait(name = pieceLayer, value = rpLayer),
            MarkTrait(name = 'Faction',  value = faction),
            MarkTrait(name = 'RP',       value = True),
            # Move to location on track
            SendtoTrait(
                name        = '',
                key         = updateRPKey,
                mapName     = board,
                boardName   = board,
                restoreName = '',
                restoreKey  = '',
                destination = SendtoTrait.REGION,
                region      = f'{{"{track} "+Current+"@{track}s"}}',
                description = f'Update replacement points'),
            # Show surplus RPs 
            LabelTrait(
                label        = f'{{({var}-Current)>0?"+"+({var}-Current):""}}',
                vertical     = LabelTraitCodes.BOTTOM,
                verticalJust = LabelTraitCodes.BOTTOM,
                horizontal   = LabelTraitCodes.RIGHT,
                horizontalJust = LabelTraitCodes.RIGHT,
                background   = rgb(255,255,255),
                foreground   = rgb(32,32,32),
                fontSize     = 20,
                fontStyle    = LabelTraitCodes.BOLD,
                nameFormat   = '$pieceName$',
                menuCommand  = '',
                propertyName = 'Surplus'),
            # Current position on the track 
            CalculatedTrait(
                name = 'Current',
                expression = f'{{({var}<0?0:({var}>10?10:{var}))}}'),
            # Increment by user 
            TriggerTrait(
                name = '',
                command = 'Increment',
                key = incrRPKey+'User',
                actionKeys=[incrRPKey+'1'],
                property = f'{{{autoReinforce}!=true}}'
            ),
            # Decrement by user 
            TriggerTrait(
                name = '',
                key = decrRPKey+'User',
                command = 'Decrement',
                actionKeys=[decrRPKey+'1'],
                property = f'{{{autoReinforce}!=true}}'
            ),
            # Print by user 
            TriggerTrait(
                name = '',
                command = 'Print',
                key = updateRPKey+'Print',
            ),
            # Change global property value 
            GlobalPropertyTrait(
                ['',incrRPKey,GlobalPropertyTrait.DIRECT,
                 f'{{{var}+1}}'],
                ['',decrRPKey,GlobalPropertyTrait.DIRECT,
                 f'{{{var}>1?{var}-1:0}}'],
                ['',capRPKey,GlobalPropertyTrait.DIRECT,
                 f'{{{var}>10?10:{var}}}'],
                name = var,
                numeric = True,
                description = f'In- or De-crement {var}'),
            # Report on increment and decrement 
            ReportTrait(
                incrRPKey,decrRPKey,
                report = (f'{{{debug}?("~{var} is now "+{var}):""}}')),
            # ReportTrait(
            #     incrRPKey+'User',decrRPKey+'User',
            #     report = (f'{{{verbose}?("User: {var} is now "+'
            #               f'{var}+" ("+Current+")"):""}}')),
            # User report 
            ReportTrait(
                updateRPKey+'Print',
                report = (f'{{{verbose}?("`{var} is "+{var}+" ("+'
                          f'Current+")"):""}}'))
        ])
        # Update on reinforcement phase 
        traits.extend([
            TriggerTrait(
                name       = 'Turn update RPs',
                command    = '', # f'Turn {t}',
                key        = incrRPTurnKey,
                actionKeys = [incrRPKey+str(n)],
                property   = (f'{{Turn=={t}&&'
                              f'Phase.contains("reinf")&&'
                              f'Phase.contains(Faction)}}'))
            for t, n in turns.items()])
        traits.append(
            ReportTrait(
                *[incrRPKey+str(n) for n in range(1,6)],
                report = (f'{{{verbose}?("!{prty} is now "+{var}):""}}')))
        if max:
            # If there's a maximum, then report that too on print
            expr = ('{Turn<10?2:3}' if tpe == 'Inf' else
                    '{Turn<7?3:(Turn<13?4:5)}')
            traits.extend([
                ReportTrait(
                    updateRPKey+'Print',
                    report = (f'{{{verbose}?("!Maximum CF for {tpe}: "'
                              +f'+{max}+" ({max})"):""}}')),
                GlobalPropertyTrait(
                    ['',incrRPTurnKey,GlobalPropertyTrait.DIRECT,expr],
                    name        = max,
                    numeric     = True,
                    description = f'Update max {nation} {tpe} max CF'),
                ReportTrait(
                    incrRPTurnKey,
                    report = (f'{{{verbose}?("!{faction} max {tpe} CF "+'
                              f'"updated to "+{max}):""}}'))])
        
        # Increments 1 to 5
        traits.extend([
            TriggerTrait(
                name       = 'Increment RPs',
                command    = '', # f'Increment {i}',
                key        = incrRPKey+str(i),
                actionKeys = [incrRPKey,updateRPKey],
                loop       = True,
                loopType   = TriggerTrait.COUNTED,
                count      = i)
            for i in range(1,6)
        ])
        # Decrements 1 to 5 
        traits.extend([
            TriggerTrait(
                name       = 'Decrement RPs',
                command    = '', # f'Decrement {i}',
                key        = rpDecrKeyGen(nation,tpe,i),
                actionKeys = [decrRPKey,updateRPKey],
                loop       = True,
                loopType   = TriggerTrait.COUNTED,
                count      = i)
            for i in range(1,6)
        ])
        # Report on specific increments and decrements
        # traits.extend([
        #     ReportTrait(*[incrRPKey+str(i) for i in range(1,6)],
        #                 report = (f'{{{debug}?("~"+PieceName+'
        #                           f'" increment RPs: "+'
        #                           f'Current):""}}')),
        #     ReportTrait(*[decrRPKey+str(i) for i in range(1,6)],
        #                 report = (f'{{{debug}?("~"+PieceName+'
        #                           f'" decrement RPs: "+'
        #                           f'Current):""}}'))])

        traits.append(basic)
        piece.setTraits(*traits)

        # Place on RP track
        dest = f'{track} 0@{track}s'
        # print(f'Adding {rpName} to board at "{dest}"')
        mainAtStart(main,dest,piece)

    # ----------------------------------------------------------------
    # Process the game turn marker
    traits = []
    
    # ----------------------------------------------------------------
    # Now remaining pieces 
    for piece in pieces:
        name    = piece['entryName'].strip()
        snation = name[:3]
        parent  = piece.getParent()
        oob     = None
        traits  = piece.getTraits()
        Faction = None
        for n in ['Allied', 'Axis']:
            factP = Trait.findTrait(traits, PrototypeTrait.ID,
                                    'name', f'{n} prototype')
            if factP is None: continue

            Faction = n


        # If this piece is an augmentation piece, ignore it 
        if name.endswith(' +') or name.endswith(' ++'):
            toRemove[piece] = parent
            continue

        # if game turn piece, ignore 
        # if name == gameTurn:
        #     continue
        
        # If this piece is a replacement point piece, then ignore it 
        if name.endswith(' rp'):
            # If not AtStart (added above), flag for removal 
            if parent and parent.TAG != AtStart.TAG:
                toRemove[piece] = parent
            continue 
            
        
        # Check for AtStart of this piece
        atStart = None
        if parent and parent.TAG == AtStart.TAG:
            atStart = parent
            pboard  = parent.getParent()
            oob     = pboard

        # This piece should be removed because it is not on the OOB
        if Faction is not None and \
           oob is None \
           and 'locomotive' not in name \
           and 'malta'      not in name :
            # print(f'Will remove {name} since not on OOB')
            toRemove[piece] = parent
            continue
        
        # If we're on the hidden piece, continue 
        if name == hidden:
            traits = piece.getTraits()
            basic  = traits.pop()
                                     
            traits.append(basic)
            piece.setTraits(*traits)
            continue

        if name == gameTurn:
            # print('Process game turn')
            traits = piece.getTraits()
            basic  = traits.pop()
            # Game turn marker 
            markers  = Trait.findTrait(traits,
                                       PrototypeTrait.ID,
                                       key = 'name',
                                       value = 'Markers prototype')
            flip     = Trait.findTrait(traits,LayerTrait.ID)
            if markers is not None:
                traits.remove(markers)

            # Victory points
            traits.extend([
                CalculatedTrait(
                    name       = f'{faction}SumCF',
                    expression = (fr'{{SumMap("CurrentCF",'
                                  fr'"{{Faction==\"{faction}\"&&'
                                  fr'CurrentZone==\"hexes\"&&'
                                  fr'Supply_Level==1}}")}}'))
                for faction in ['Axis','Allied']])
            traits.extend([
                CalculatedTrait(
                    name       = f'{faction}VPs',
                    expression = (f'{{{faction}SumCF'
                                  f'-Math.ceil(1.5*{faction}{vpPenalty})}}'))
                for faction in ['Axis','Allied']])
            ctrl = [f'" {name}: "+{name.replace(" ","")+"Control"}+" ("+'
                    f'{name.replace(" ","")+"Count"}+")"'
                    for name in objectiveNames.values()]
            ctrlPty = [f'"` {name} | "+'
                       f'{name.replace(" ","")+"Control"}+" || "+'
                       f'{name.replace(" ","")+"Count"}+" @ "'
                       for name in objectiveNames.values()]
            traits.extend([
                TriggerTrait(
                    name = '',
                    command = '',
                    key = checkVictoryKey,
                    actionKeys = [checkVictoryKey+'Set',
                                  checkVictoryKey+'Show']),
                GlobalPropertyTrait(
                    ['',checkVictoryKey+'Set',
                     GlobalPropertyTrait.DIRECT,
                     f'{{"` Faction | VP | CF | Penalty @ "+'
                     f'"` Axis | "+AxisVPs+'
                     f'" | "+AxisSumCF+'
                     f'" | "+(1.5*Axis{vpPenalty})+" @ "+'
                     f'"` Allied | "+AlliedVPs+'
                     f'" | "+AlliedSumCF+'
                     f'" | "+(1.5*Allied{vpPenalty})+" @ "+'
                     fr'"` Allied\/Axis | "+(AlliedVPs\/AxisVPs)+" || @ "+'
                     f'"` City | Owner || Turns @ "+'
                     f'{"+".join(ctrlPty)}+'
                     f'"`` Italian infantry losses || "+{itInfantryLoss}+" @"'
                     f'}}'],
                    name = victoryPretty,
                    description = 'Set pretty victory string'),
                GlobalHotkeyTrait(
                    name = '',
                    key  = checkVictoryKey+'Show',
                    globalHotkey = checkVictoryKey+'Show',
                    description = 'Trampoline to global'),
                ReportTrait(
                    checkVictoryKey+'Show',
                    report = (f'{{{debug}?("~Current VPs "+'
                              f'" Axis="+AxisVPs+" ("+AxisSumCF+'
                              f'"-1.5*"+Axis{vpPenalty}+")"+'
                              f'" Allied="+AlliedVPs+'
                              f'" ("+AlliedSumCF+'
                              f'"-1.5*"+Allied{vpPenalty}+")"+'
                              f'{"+".join(ctrl)}+'
                              f'" => "+{victoryPretty}'
                              f'):""}}'))])

            
            # Malta invasion
            traits.extend([
                PrototypeTrait(name='Dice prototype'),
                CalculatedTrait(
                    name = 'AlliedInLibya',
                    expression = (r'{CountMap("{InLibya==true&&'
                                  r'Faction==\"Allied\"&&'
                                  r'Supply_Level==1}")}')),
                TriggerTrait(
                    command    = 'Invasion',
                    key        = maltaInvasionKey,
                    actionKeys = [dieRoll,
                                  maltaInvasionKey+'Take',
                                  maltaInvasionKey+'Disable',
                                  maltaInvasionKey+'Move'],
                    property = f'{{{maltaStatus}==0}}'
                ),
                #0:no attempt,1:success,2:failure
                GlobalPropertyTrait(
                    ['',maltaInvasionKey+'Take',
                     GlobalPropertyTrait.DIRECT,
                     f'{{((Die<=4&&AlliedInLibya<=0)||'
                     f'(Die<=2&&AlliedInLibya>0))?1:2}}'],
                    name = maltaStatus),
                GlobalPropertyTrait(
                    ['',maltaInvasionKey+'Disable',GlobalPropertyTrait.DIRECT,
                     '{true}'],
                    name        = maltaNotPossible,
                    numeric     = True,
                    description = 'Disalbe Malta invasion attempt'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = maltaInvasionKey+'Move',
                    globalHotkey = maltaInvasionKey+'Move',
                    description  = 'Trampoline to global'),
                ReportTrait(
                    maltaInvasionKey+'Take',
                    report = (f'{{({maltaStatus}==1)?'
                              f'"!Axis invasion of Malta successful":'
                              f'{maltaStatus}==2?'
                              f'"`Axis invasion of Malta failed":'
                              f'"?No Axis attempt at invading Malta"}}')),
                # TriggerTrait(
                #     command = 'Print',
                #     key = printKey),
                # ReportTrait(
                #     printKey,
                #     report = f'{{{verbose}?("`"+AlliedInLibya):""}}')
                ])

            # C2 
            traits.extend([
                TriggerTrait(
                    name       = 'Check for C2',
                    command    = '',
                    key        = checkC2,
                    actionKeys = [checkC2+'Reset',
                                  checkC2+'Die',
                                  checkC2+'Last',
                                  checkC2+'Trampoline'],
                    property  = f'{{{autoC2}==true}}'
                ),
                TriggerTrait(
                    name = 'Roll one die for C2',
                    command = '',
                    key     = checkC2+'Die',
                    actionKeys = [dieRoll],
                    property   = f'{{{c2OneRoll}==true}}'),
                GlobalPropertyTrait(
                    ['',checkC2+'Last', GlobalPropertyTrait.DIRECT,
                     f'{{(Die==1)?":1:7:3:":'
                     f'(Die==2)?":2:8:4:":'
                     f'(Die==3)?":3:5:9:":'
                     f'(Die==4)?":4:6:0:":'
                     f'(Die==5)?":5:1:7:":'
                     f'(Die==6)?":6:2:0:":""'
                     f'}}'],
                    name = c2LastDigits,
                    description = 'Set last digits for C2 - SPI style'),
                GlobalPropertyTrait(
                    ['',checkC2+'Reset',GlobalPropertyTrait.DIRECT,'{}'],
                    name = c2Hexes,
                    description = 'Reset C2 hexes'),
                GlobalHotkeyTrait(
                    name = '',
                    key  = checkC2+'Trampoline',
                    globalHotkey = checkC2+'Real',
                    description  = 'Trampoline to global'),
                ReportTrait(
                    checkC2,
                    report = (f'{{{debug}?("~"+BasicName+" Start C2"):""}}')),
                ReportTrait(
                    checkC2+'Reset',
                    report = (f'{{{debug}?("~"+BasicName+" Reset C2"):""}}')),
                ReportTrait(
                    checkC2+'Last',
                    report = (f'{{{debug}?("~"+BasicName+'
                              f'"C2 last digits: "+{c2LastDigits}+" "+'
                              f'Die):""}}')),
                ReportTrait(
                    checkC2+'Trampoline',
                    report = (f'{{{debug}?("~"+BasicName+'
                              f'" Trampoline C2"):""}}'))
            ])
            traits.extend([
                GlobalHotkeyTrait(
                    key          = key('O',ALT),
                    globalHotkey = key('O',ALT),
                    description = 'Open scenario options'),
                ReportTrait(
                    key('O',ALT),
                    report = (f'{{{debug}?("~"+BasicName+'
                              f'"Show scenario options"):""}}'))
            ])
            
            # Add the prototypes we created above 
            traits.extend([PrototypeTrait(pnn + ' prototype')
                           for pnn in turnp])
            # Do not allow movement
            traits.append(RestrictAccessTrait(sides=[],
                                              description='Cannot move'))
            traits.append(basic)
            piece.setTraits(*traits)

            main.addAtStart(name            = 'Game turn',
                            location        = 'Turn 1@turns',
                            useGridLocation = True,
                            owningBoard     = main['mapName']).\
                            addPiece(piece)
            continue 

        if name == 'de malta':
            basic = traits.pop()

            traits = [
                NoStackTrait(
                    select      = NoStackTrait.NORMAL_SELECT,
                    bandSelect  = NoStackTrait.NEVER_BAND_SELECT,
                    move        = NoStackTrait.NEVER_MOVE,
                    canStack    = False,
                    ignoreGrid  = False,
                    description = 'Restrict movement of Malta marker'),
                ClickTrait(context = True),
                TriggerTrait(
                    name         = '',
                    key          = maltaKey,
                    command      = 'Attempt invasion of Malta',
                    actionKeys   = [maltaInvasionKey+'Trampoline'],
                    property     = f'{{{maltaNotPossible}!=true}}'),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = maltaInvasionKey+'Trampoline',
                    globalHotkey = maltaKey,
                    description  = 'Send global hot key'),
                ReportTrait(
                    maltaInvasionKey+'Trampoline',
                    report = (f'{{{debug}?("~Malta invasion from marker"):'
                              f'""}}')),
                TriggerTrait(
                    name         = 'Send to success',
                    command      = '',
                    key          = maltaInvasionKey+'Move',
                    actionKeys   = [maltaInvasionKey+'Success'],
                    property     = f'{{{maltaStatus}==1}}'),
                TriggerTrait(
                    name         = 'Send to failure',
                    command      = '',
                    key          = maltaInvasionKey+'Move',
                    actionKeys   = [maltaInvasionKey+'Failure'],
                    property     = f'{{{maltaStatus}==2}}'),
                SendtoTrait(mapName     = 'Axis OOB',
                            boardName   = 'Axis OOB',
                            name        = '',
                            restoreName = '',
                            restoreKey  = '',
                            zone        = 'Axis malta',
                            destination = 'R', #R for region
                            region      = 'de malta success',
                            key         = maltaInvasionKey+'Success',
                            x           = 0,
                            y           = 0),
                SendtoTrait(mapName     = 'Axis OOB',
                            boardName   = 'Axis OOB',
                            name        = '',
                            restoreName = '',
                            restoreKey  = '',
                            zone        = 'Axis malta',
                            destination = 'R', #R for region
                            region      = 'de malta failure',
                            key         = maltaInvasionKey+'Failure',
                            x           = 0,
                            y           = 0),
                basic
            ]
            piece.setTraits(*traits)
            continue
        
        if name.startswith('reinf'):
            opt     = name[-1].upper()
            if opt == 'M':
                continue
            
            faction = 'Allied' if  ord(opt) < ord('F') else 'Axis'
            basic   = traits.pop()

            # Malta status:
            # - 0, No attempt,
            # - 1, Success,
            # - 2, Failure
            status  = f'ReinforceOpt{opt}'
            if opt in ['H','J']:
                # Disable on malta failure 
                status = f'{maltaStatus}>=2?-1:ReinforceOpt{opt}'
            if opt in ['L']:
                # Disalbe on malta success or failure 
                status = f'{maltaStatus}>=1?-1:ReinforceOpt{opt}'
            traits  = [
                PrototypeTrait(name = f'{faction} reinforce prototype'),
                CalculatedTrait(name = 'Status',
                                expression = f'{{{status}}}'),
                GlobalPropertyTrait(
                    ['',optReinfKey+'Select',
                     GlobalPropertyTrait.DIRECT, '{1}'],
                    name = f'ReinforceOpt{opt}',
                    numeric = True)
            ]
            optTurns = {'A': [8],
                        'B': [13,15],
                        'C': [15],
                        'D': [14],
                        'E': [17],
                        #
                        'F': [16,None],
                        'G': [16,None],
                        'H': [16,None],
                        'J': [16,None],
                        'K': [16,None],
                        'L': [16,None]}.get(opt,None)
            if optTurns:
                if len(optTurns) == 1:
                    traits.append(
                        CalculatedTrait(
                            name       = 'TurnOK',
                            expression = f'{{Turn=={optTurns[0]}}}'))
                elif optTurns[-1] == None:
                    traits.append(
                        CalculatedTrait(
                            name       = 'TurnOK',
                            expression = f'{{Turn>={optTurns[0]}}}'))
                else:
                    sel = '||'.join([f'Turn=={o}' for o in optTurns])
                    traits.append(
                        CalculatedTrait(
                            name       = 'TurnOK',
                            expression = f'{{{sel}}}'))
                        
            dkeys = []
            if opt in ['B','C','D']:
                dkeys.extend([optReinfKey+'Disable'+o
                              for o in ['B','C','D'] if o != opt])
                traits.extend([
                    GlobalPropertyTrait(
                        ['',optReinfKey+'Disable'+o,
                         GlobalPropertyTrait.DIRECT, '{-1}'],
                        name = f'ReinforceOpt{o}')
                    for o in ['B','C','D'] if o != opt])

            if opt == 'F':
                dkeys.extend([optReinfKey+'Disable'+'G',
                              optReinfKey+'Disable'+'K'])
                traits.extend([
                    GlobalPropertyTrait(
                        ['',dkeys[-2],
                         GlobalPropertyTrait.DIRECT,
                         f'{{({maltaStatus}==0||{maltaStatus}==2)?-1:'
                         f'ReinforceOptG}}'],
                        name = f'ReinforceOptG'),
                    GlobalPropertyTrait(
                        ['',dkeys[-1],
                         GlobalPropertyTrait.DIRECT,
                         f'{{({maltaStatus}==2)?-1:'
                         f'ReinforceOptK}}'],
                        name = f'ReinforceOptK')
                ])
            if opt == 'G':
                dkeys.extend([optReinfKey+'Disable'+'F',
                              optReinfKey+'Disable'+'K'])
                traits.extend([
                    GlobalPropertyTrait(
                        ['',dkeys[-2],
                         GlobalPropertyTrait.DIRECT,
                         f'{{({maltaStatus}==0||{maltaStatus}==2)?-1:'
                         f'ReinforceOptF}}'],
                        name = f'ReinforceOptF'),
                    GlobalPropertyTrait(
                        ['',dkeys[-1],
                         GlobalPropertyTrait.DIRECT,
                         f'{{({maltaStatus}==2)?-1:'
                         f'ReinforceOptK}}'],
                        name = f'ReinforceOptK')
                ])
            if opt in ['H','J','K','L']:
                dkeys.extend([optReinfKey+'Disable'+o
                              for o in ['H','J','K','L']
                              if o != opt])
                traits.extend([
                    GlobalPropertyTrait(
                        ['',optReinfKey+'Disable'+o,
                         GlobalPropertyTrait.DIRECT,
                         f'{{({maltaStatus}==0)?-1:'
                         f'ReinforceOpt{o}}}'],
                        name = f'ReinforceOpt{o}')
                    for o in ['H','J','K','L'] if o != opt])

            traits.append(
                TriggerTrait(
                    name       = 'Disable others',
                    command    = '',
                    key        = optReinfKey+'Disable',
                    actionKeys = dkeys))

            traits.append(basic)
            piece.setTraits(*traits)
            continue

        if name == 'small control':
            flipCtrl  = key(NONE,0)+',flipControl'
            traits = piece.getTraits()
            basic  = traits.pop()
            rept   = Trait.findTrait(traits, ReportTrait.ID)
            empt   = Trait.findTrait(traits, PrototypeTrait.ID,
                                     'name', ' prototype')
            step   = Trait.findTrait(traits, LayerTrait.ID,'name', 'Step')
            step['increaseKey']  = flipCtrl
            step['increaseName'] = ''
            step['resetKey']     = ''
            step['newNames']     = ','.join(['Axis control',
                                             'Allied control'])
            traits.remove(rept)
            traits.remove(empt)
            
            traits.extend([
                TriggerTrait(
                    name       = 'Flip the control',
                    command    = 'Flip',
                    key        = stepKey,
                    actionKeys = [flipCtrl,flipCtrl+'Global']),
                CalculatedTrait(
                    name = 'Controlling',
                    expression = '{Step_Level==1?"Axis":"Allied"}'),
                GlobalPropertyTrait(
                    ['',flipCtrl+'Global',GlobalPropertyTrait.DIRECT,
                     '{Controlling}'],
                    name = 'TBD'),
                GlobalPropertyTrait(
                    ['',flipCtrl+'Global',GlobalPropertyTrait.DIRECT,
                     '{0}'],
                    ['',countIncrKey,GlobalPropertyTrait.INCREMENT,'{1}'],
                    name = 'TBDCnt'),
                ReportTrait(
                    flipCtrl+'Global',
                    report = (f'{{{verbose}?("!"+Controlling+'
                              f'" now controls "+Objective+" ("+'
                              f'Global+")"):""}}')),
                #TriggerTrait(
                #    name = 'Print',
                #    command = 'Print',
                #    key     = printKey),
                #ReportTrait(
                #    printKey,
                #    report = (f'{{{verbose}?("!"+PieceName+'
                #              f'" Location="+LocationName+'
                #              f'" Controlling="+Controlling+'
                #              f'" Objective="+Objective+'
                #              f'" Global="+Global+'
                #              f'" Step="+Step_Level'
                #              f'):""}}'))
                basic
            ])
            
            piece.setTraits(*traits)


            for ctrl,prty in objectiveNames.items():
                dest = hexName(ctrl)
                glbl = prty.replace(' ','')+'Control'
                cnt  = prty.replace(' ','')+'Count'
                # print(f'Adding control marker at {dest} -> {prty}')
                at = main.addAtStart(name            = dest,
                                     location        = dest,
                                     owningBoard     = board,
                                     useGridLocation = True)
                cp = at.addPiece(piece)
                ctraits = cp.getTraits()
                cbasic  = ctraits.pop()
                cstep   = Trait.findTrait(ctraits,LayerTrait.ID,'name','Step')
                cset    = Trait.findTrait(ctraits,GlobalPropertyTrait.ID,
                                          'name','TBD')
                ccnt    = Trait.findTrait(ctraits,GlobalPropertyTrait.ID,
                                          'name','TBDCnt')
                cstep.setState(level=1 if ctrl == elAgheilaC else 2)
                cstep['newNames'] = ','.join([f'Axis control of {prty}',
                                              f'Allied control of {prty}'])
                
                cset['name'] = glbl
                ccnt['name'] = cnt
                
                ctraits.extend([
                    MarkTrait(name='Objective',value=prty),
                    CalculatedTrait(name='Global',
                                    expression=f'{{{glbl}}}'),
                    cbasic])
                cp.setTraits(*ctraits)

            continue

        # Odds markers
        if 'odds marker' in name:
            odds = int(name.split(' ')[-1])
            # print(name,odds)
            if odds == -1:
                expr = '0'
            elif odds == 11:
                expr = '11'
            else:
                row  = crt[odds]
                expr = (':'.join([f'Die=={n+1}?{r}'
                                  for n,r in enumerate(row)]) +':0')
            #print(name,odds,row,expr)

            traits = piece.getTraits()
            basic  = traits.pop()
            traits.append(
                CalculatedTrait(
                    name = battleResult,
                    expression = f'{{{expr}}}'))
            traits.append(basic)
            piece.setTraits(*traits)
            continue
        
        # Something else        
        if Faction is None:
            continue

        basic   = traits.pop()
        step    = Trait.findTrait(traits, LayerTrait.ID,'name', 'Step')
        eprot   = Trait.findTrait(traits, PrototypeTrait.ID,
                                  'name',' prototype')
        if eprot is not None: traits.remove(eprot)

        nation  = preToNation[name[:2]]
            
        if 'supply' in name or \
           'lorry' in name or \
           'locomotive' in name or \
           'malta' in name:
            # Remove faction prototype from markers 
            fprot = Trait.findTrait(traits,
                                    PrototypeTrait.ID,
                                    'name', f'{Faction} prototype')
            if fprot: traits.remove(fprot)
            # Put on unit layer
            if 'lorry' in name or 'locomotive' in name:
                traits.append(
                    PrototypeTrait(name=f'{Faction} lorry prototype'))
            elif 'supply' in name:
                traits.append(
                    PrototypeTrait(name=f'{Faction} supply prototype'))

        # print(f'{name:20s} {Faction} {snation} {parent}')

        inf  = [Trait.findTrait(traits,PrototypeTrait.ID,
                                'name', v+' prototype')
                for v in revTypes['Infantry']]
        arm  = [Trait.findTrait(traits,PrototypeTrait.ID,
                                'name', v+' prototype')
                for v in revTypes['Armoured']]
        tpe  = 'Arm' if any(arm) else 'Inf' if any(inf) else None
        rp   = nation+tpe+'RP' if tpe else None
            
        if step is not None:
            #traits.remove(step)
            step['loop']         = ''
            step['increaseKey']  = ''
            step['increaseName'] = ''
            step['resetKey']     = ''
            step['follow']       = True
            step['expression'] = '{Flipped==true?2:1}'
                
        hx                   = ''
        re                   = ''
        tn                   = -1
        for trait in traits:
            if trait.ID == MarkTrait.ID:
                if hx == '' and trait['name'] == 'upper right':
                    st = trait['value']
                    st = sub('[^=]+=','',st).strip()
                    hx = st
                    if len(hx) < 4:
                        hx, re = '', hx
                if trait['name'] == 'upper left':
                    st  = trait['value']
                    st2 = sub('[^=]+=','',st).strip()
                    try:
                        tn = int(st2)
                    except:
                        tn = -1
                        re = st2

        if hx != '' and atStart:
            # print(f'--> Removing {name} from {pboard}')
            pboard.remove(atStart)
            destName = (hexName(hx) if hx.lower()
                        != 'cadre' else f'{Faction} cadre')
            
            mainAtStart(main,destName,piece)

        if hx != '' and 'locomotive' in name:
            #print(f'Adding {name} to {hx}')
            mainAtStart(main,hexName(hx),piece)
            

        if name in ['it 185 abiregt',
                    'it 186 abiregt',
                    'it 187 abiregt',
                    'de ram abibde']:
            re = 'M'
            
        #if re != '':
        # print(f'{name:30s} {re:1s} {hx:4s}')
            
        if tn != '':
            if re == 'M':
                # Make reinforcement with some Axis units contingent on
                # the state of the Malta invasion attempt 
                traits.append(
                    TriggerTrait(
                        name = f'Reinforce with {name}',
                        command = '',
                        key = reinforceKey,
                        actionKeys = [reinforceKey+'Send'],
                        property   = (f'{{(CurrentBoard!="{board}")&&('
                                      f'({maltaStatus}==0&&Turn=={tn})||'
                                      f'({maltaStatus}==1&&Turn==18)'
                                      f')}}')))
            elif 'supply' not in name:
                # Reinforce on specified turn Here, we need to take
                # the optional reinforcement into account.  We need to
                # set up a global property for each reinforcement
                # option, and then check if that option has been
                # enabled.  This should be done by flicking the
                # reinforcement counter _before_ the reinforcement
                # turn.  Optionally, we can send a key command when
                # the optional reinforcement is clicked to send the
                # affected units in as reinforcements.  This later
                # option will be the best one.
                traits.append(
                    # Set value of reinforcement turn from global
                    # property `ReenterTurn`
                    DynamicPropertyTrait(
                        ['',reinforceKey+'Turn',DynamicPropertyTrait.DIRECT,
                         '{ReenterTurn}'],
                        name = 'ReinforceTurn',
                        numeric = True,
                        value   = int(tn)
                    ))
                if re == '':
                    traits.extend([
                        TriggerTrait(
                            name       = f'Reinforce with {name}',
                            command    = '',
                            key        = reinforceKey,
                            actionKeys = [reinforceKey+'Send'],
                            property   = (f'{{Turn==ReinforceTurn'
                                          f'&&CurrentBoard!="{board}"}}'))
                    ])
                else:
                    traits.extend([
                        TriggerTrait(
                            name       = f'Reinforce with {name}',
                            command    = '',
                            key        = reinforceKey,
                            actionKeys = [reinforceKey+'Penalty',
                                          reinforceKey+'Send'],
                            property   = (f'{{Turn>=ReinforceTurn'
                                          f'&&ReinforceOpt{re}>=1'
                                          f'&&CurrentBoard!="{board}"}}')),
                        GlobalPropertyTrait(
                            ['',reinforceKey+'Penalty',
                             GlobalPropertyTrait.INCREMENT,
                             '{CurrentCF}'],
                            name = Faction+vpPenalty),
                        ReportTrait(
                            reinforceKey+'Penalty',
                            report = (f'{{{verbose}?("!Reinforce by"+'
                                      f'PrettyName+" costs "+'
                                      f'(1.5*CF)+" VPs -> "+(1.5*'
                                      f'{faction+vpPenalty})):""}}'))])
                    

        # add replacement command
        if nation and tpe:
            cft        = Trait.findTrait(traits,MarkTrait.ID,'name','CF')
            n          = 0
            if cft:
                scft = cft['value']
                n    = int(scft)

            # print(f'{name:20s} -> {tpe:10s} {n}')
            replaceRPKey = rpDecrKeyGen(nation,tpe,n)
            traits.extend([
                MarkTrait(name = 'Nation', value = nation),
                TriggerTrait(
                    name       = 'Replace from cadre',
                    command    = 'Replace',
                    key        = replaceKey,
                    actionKeys = [reinforceKey+'Send',decrRPKey+'Replace'],
                    property   = (f'{{Phase.contains(Faction)&&'
                                  f'Phase.contains("reinf")&&'
                                  f'{rp}>=CF&&'
                                  f'CurrentZone=="{Faction} cadre"}}')),
                GlobalHotkeyTrait(
                    key          = decrRPKey+'Replace',
                    globalHotkey = replaceRPKey,
                    description  = 'Decrement RPs'),
                ReportTrait(
                    replaceKey,
                    report = (f'{{{verbose}?("!"+PrettyName+" is replaced "+'
                              f'"from "+Faction+" cadre, at {n} RPs"):""}}'))
            ])
            traits.append(
                MarkTrait(
                    name = 'CounterFactor',
                    value = f'{4 if nation=="German" else 2}'))

            if nation == 'Italian' and tpe == 'Inf':
                traits.append(
                    PrototypeTrait(name = 'Italian infantry prototype'))
                
        # rest = RestrictCommandsTrait(
        #     'Restrict step command',
        #     hideOrDisable = 'Disable',
        #     expression    = (
        #         f'{{!Phase.contains("combat")&&'
        #         f'!Phase.contains("artillery")&&'
        #         f'(Turn%8!=0)}}'),
        #     keys       = [stepKey])
        # traits.insert(0,rest)

        if Faction == 'Allied':
            # See if we can find augmented versions 
            imgs = [] if step is None else step['images'].split(',')
            nms  = [] if step is None else step['newNames'].split(',')
            # print(inf,imgs)
            for a,nn in zip(['+','++'],['Reinforced','Twice reinforced']):
                # Search for piece 
                ap = pieces_dict.get(name+f' {a}',None)
                if ap is not None:
                    # If found, get front and back images from layer trait. 
                    astep = Trait.findTrait(ap.getTraits(), LayerTrait.ID,
                                            'name', 'Step')
                    if astep:
                        # If we found the layer trait, get images. 
                        imgs.extend(astep['images'].split(','))
                        nms.extend([f'{nn} +',f'Reduced {nn} +'])


            # calculated property to get the right step level
            # traits.append(
            #     CalculatedTrait(
            #         name = 'AlliedStep',
            #         expression = '{1+2*AugmentStatus+(Flipped==true?1:0)}')
            # )
                        
            if step is not None:
                step['images'] = ','.join(imgs)
                step['newNames'] = ','.join(nms)
                step['expression'] = '{1+2*AugmentStatus+(Flipped==true?1:0)}'
                            

            maxCF = (globalMaxArm if any(arm) else
                     globalMaxInf if any(inf) else None)
            if maxCF is not None and nation and tpe:
                traits.extend([
                    TriggerTrait(
                        name       = 'Augment',
                        command    = 'Reinforce',
                        key        = augKey,
                        actionKeys = [augStepKey,
                                      decrRPKey+'Augment'],
                        property   = (f'{{InAlexandria&&'
                                      f'Phase=="{alReinf}"&&'
                                      f'{maxCF}>CurrentCF&&'
                                      f'{rp}>0}}')),
                    GlobalHotkeyTrait(
                        key          = decrRPKey+'Augment',
                        globalHotkey = rpDecrKeyGen(nation,tpe,1),
                        description  = 'Decrement RPs'),
                    ReportTrait(
                        augStepKey,
                        report = (f'{{{verbose}?("!"+PrettyName+'
                                  f'" reinforce -> "+CurrentCF+" ("+'
                                  f'AugmentStatus+" "+'
                                  f'Step_Level+")"):""}}'))])

        pretty = makePretty(name)
        # print(f'{name:30s} -> {pretty}')
            
        traits.append(MarkTrait(name='PrettyName',
                                value=pretty))

        # Set traits 
        traits.append(basic)
        piece.setTraits(*traits)


    for piece,parent in toRemove.items():
        # print(f'Removing {piece["entryName"]} from {parent}')
        parent.remove(piece)
        
    # --- Remove piece window - not needed ---------------------------
    pwindows = game.getPieceWindows()
    for pw in pwindows.values():
        # print(f'Piece window: {pw["name"]}')
        tab = pw.getTabs().get("Counters",None)
        if not tab:
            continue

        toRemove = ['Allied',
                    'Axis',
                    'ChoiceMarkers',
                    ]
        panels   = tab.getPanels()
        for panName, panel in panels.items():
            if panName in toRemove:
                tab.remove(panel)
    
#
# EOF
#
