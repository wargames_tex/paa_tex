#
#
#
NAME		:= PanzerarmeeAfrika
VERSION		:= 1.0
VMOD		:= $(NAME).vmod
VMOD_VERSION	:= 1.1.0
VMOD_TITLE	:= Panzerarmee Afrika
VASSAL_VERSION	:= 3.7.0
TUTORIAL	:= Tutorial.vlog

LATEX		:= lualatex

CONVERT		:= convert
CONVERT_FLAGS	:= -trim
RCONVERT_FLAGS	:= $(CONVERT_FLAGS) -rotate 90

IMGS		:= .imgs

DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			
DATAFILES	:= hexes.tex			\
		   oob.tex			\
		   chits.tex			\
		   tables.tex

OTHER		:= 
STYFILES	:= paa.sty			\
		   commonwg.sty
BOARD		:= hexes.pdf
BOARDS		:= splitboardA4.pdf		
LBOARDS		:= splitboardLetter.pdf	
SIGNATURE	:= 24
PAPER		:= --a4paper
TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   $(NAME)A3Booklet.pdf 	\
		   materialsA4.pdf		\
		   $(BOARD)			\
		   splitboardA3.pdf		\
		   splitboardA4.pdf		

TARGETSLETTER	:= $(subst A3,Tabloid, $(subst A4,Letter, $(TARGETSA4)))

include Variables.mk
include Patterns.mk

all:	a4 letter vmod 
a4:	$(TARGETSA4) vmod
letter:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
mine:	a4 cover.pdf

spi:
	$(MAKE) clean 
	@echo "Will create SPI variant"
	$(MUTE) touch .spi
	$(MAKE) $(filter-out spi, $(MAKECMDGOALS))
	$(MUTE)rm -f .spi

ahgc:
	$(MAKE) clean 
	@echo "Will create AHGC variant"
	$(MUTE) touch .ahgc
	$(MAKE) $(filter-out ahgc, $(MAKECMDGOALS))
	$(MUTE)rm -f .ahgc

show:
	@echo "A4:"
	@$(foreach t, $(TARGETSA4),echo "\t$(t)"; )
	@echo "Letter:"
	@$(foreach t, $(TARGETSLETTER),echo "\t$(t)"; )

include Rules.mk

clean::
	$(MUTE)rm -f .ahgc .spi

realclean::
	$(MUTE)rm -f *.vtmp
	$(MUTE)rm -rf $(NAME)-*-$(VERSION)+*

$(IMGS)/hexes.png:hexes.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

$(IMGS)/tables.png:tables.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

$(IMGS)/front.png:front.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

$(IMGS)/logo.png:logo.png
	@echo "CONVERT	$<"
	mv $< $@

$(IMGS)/al-oob.png:oobA4.pdf
	@echo "PDFTOCAIRO	$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	@echo "CONVERT		$<"
	$(MUTE)$(CONVERT) oobA4-1.png $(RCONVERT_FLAGS) $(IMGS)/al-oob.png
	$(MUTE)$(CONVERT) oobA4-2.png $(RCONVERT_FLAGS) $(IMGS)/ax-oob.png

$(IMGS)/al-chits.png:chitsA4.pdf
	@echo "PDFTOCAIRO	$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	@echo "CONVERT		$<"
	$(MUTE)$(CONVERT) chitsA4-3.png $(CONVERT_FLAGS) $(IMGS)/al-chits.png
	$(MUTE)$(CONVERT) chitsA4-2.png $(CONVERT_FLAGS) $(IMGS)/ax-chits.png
	$(MUTE)$(CONVERT) chitsA4-1.png $(CONVERT_FLAGS) $(IMGS)/cm-chits.png

update-images:	$(IMGS)/al-chits.png 	\
		$(IMGS)/hexes.png 	\
		$(IMGS)/al-oob.png 	\
		$(IMGS)/tables.png	\
		$(IMGS)/front.png	\
		$(IMGS)/logo.png

update-images-spi:
	mkdir -p $(IMGS)+SPI
	$(MAKE) spi update-images IMGS:=$(IMGS)+SPI
	$(MUTE)rm -f $(IMGS)+SPI/logo.png
	$(MUTE)for i in $(IMGS)+SPI/*.png ; do \
	  mv $$i $(IMGS)/`basename $$i .png`+SPI.png ; done
	$(MUTE)rm -rf $(IMGS)+SPI


update-images-ahgc:
	mkdir -p $(IMGS)+AHGC
	$(MAKE) ahgc update-images IMGS:=$(IMGS)+AHGC
	$(MUTE)rm -f $(IMGS)+AHGC/logo.png
	$(MUTE)for i in $(IMGS)+AHGC/*.png ; do \
	  mv $$i $(IMGS)/`basename $$i .png`+AHGC.png ; done
	$(MUTE)rm -rf $(IMGS)+AHGC

# These generate images that are common for all formats 
export.pdf:		export.tex	$(DATAFILES)	$(STYFILES) 

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES) 	$(BOARDS)	\
			materialsA4.pdf
materialsA4.pdf:	materials.tex 	$(DATAFILES) 	$(BOARDS)	
oobA4.pdf:		oob.tex		$(DATAFILES)	$(STYFILES)
chitsA4.pdf:		chits.tex       		$(STYFILES)
tablesA4.pdf:		tables.tex	$(DATAFILES)	$(STYFILES)
frontA4.pdf:		front.tex	$(DATAFILES)	$(STYFILES) 	

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	$(LBOARDS)\
			materialsLetter.pdf
materialsLetter.pdf:	materials.tex 	$(DATAFILES) 	$(LBOARDS)
oobLetter.pdf:		oob.tex		$(DATAFILES)	$(STYFILES)
chitsLetter.pdf:	chits.tex       		$(STYFILES)
tablesLetter.pdf:	tables.tex	$(DATAFILES)	$(STYFILES)

$(NAME)LetterBooklet.pdf:PAPER=--letterpaper

cover.pdf:		cover.tex	$(BOARD)

distdirA4SPI:
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)+SPI"
	$(MUTE)$(MAKE) spi a4 VMOD_TITLE:="$(VMOD_TITLE) - SPI"
	$(MUTE)$(MAKE) distdirA4 VERSION:=$(VERSION)+SPI

distdirA4AHGC:
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)+AHGC"
	$(MUTE)$(MAKE) ahgc a4 VMOD_TITLE:="$(VMOD_TITLE) - AHGC"
	$(MUTE)$(MAKE) distdirA4 VERSION:=$(VERSION)+AHGC

distdirLetterSPI:
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)+SPI"
	$(MUTE)$(MAKE) spi letter VMOD_TITLE:="$(VMOD_TITLE) - SPI"
	$(MUTE)$(MAKE) distdirLetter VERSION:=$(VERSION)+SPI

distdirLetterAHGC:
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)+AHGC"
	$(MUTE)$(MAKE) ahgc letter VMOD_TITLE:="$(VMOD_TITLE) - AHGC"
	$(MUTE)$(MAKE) distdirLetter VERSION:=$(VERSION)+AHGC

include Docker.mk

docker-artifacts::	distdirA4SPI		\
			distdirA4AHGC		\
			distdirLetterSPI	\
			distdirLetterAHGC	
	@echo "VMOD dist"

.PRECIOUS:	export.pdf 		\
		$(NAME)Rules.pdf	\
		$(NAME).vtmp		\
		splitboardA4.tex	\
		splitboardA3.tex	\
		splitboardLetter.tex	\
		splitboardTabloid.tex	\
		materialsA4.pdf		\
		materialsLetter.pdf	

#
# EOF
#

