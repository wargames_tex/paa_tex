# Panzerarmee Afrika

[[_TOC_]]

This is my remake of the wargame _Panzerarmee Afrika_. The original
game was published first in 1973 by Simulations Publications,
Inc. (SPI), and later, in 1982, by the Avalon Hill Game Company
(AHGC).  The SPI original materials can be found at
[`spigames`][spigames] or [`simpubs`][simpubs]).

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | WWII               |
| Level        | Operational        |
| Hex scale    | 19.3 km (12 miles) |
| Unit scale   | regiment (\|\|\|)  |
| Turn scale   | 1 month            |
| Unit density | low                |
| # of turns   | 20                 |
| Complexity   | 4 of 10            |
| Solitaire    | 8 of 10            |

Features:
- Combat differentials 
- Counter attacks 
- Logistics 
- High mobility 

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules. 


## The files 

The distribution consists of the following files 

- [PanzerarmeeAfrikaA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into eight parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
- [PanzerarmeeAfrikaA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge and stabled to form an 24-page A5
  booklet of the rules.
  
- [PanzerarmeeAfrikaA3Booklet.pdf][] As above, but designed to be
  printed in an A3 duplex printer.  This will give 6 A3 sheets to form
  a 24-page A4 booklet.
  
- [splitboardA3.pdf][] holds the board on four A3 sheets.  Print
  this and glue on to a sturdy piece of cardboard (1.5mm poster carton
  seems a good choice).  Note that one sheets should be cut at the crop
  marks and glued over the other sheet.  Take care to align the
  sheets.
  
- [splitboardA4.pdf][] has the board split over eight A4 sheets.  Cut
  out and glue on to sturdy cardboard.  Note that one sheets should be
  cut at the crop marks and glued over the other sheet.  Take care to
  align the sheets.
  
- [materialsA4.pdf][] holds the charts, counters, OOBs, and board.  It
  is meant to be printed on A4 paper.  Print and glue on to a
  relatively thick piece of cardboard (1.5mm or so) or the like.
  
- [hexes.pdf][] is the full board as a single PDF.  It is 92.3cm x
  37.5cm (36.3" x 14.8") big, so you need a special printer to use
  this. 
  
You may want to checkout [this
page](https://wargames_tex.gitlab.io/wargame_www/howto.html) for some
hints on how to use the PDFs to craft your own game. 
    
If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                        | *Letter Series*                         |
| -----------------------------------|-----------------------------------------|
| [PanzerarmeeAfrikaA4.pdf][]        | [PanzerarmeeAfrikaLetter.pdf][]  	   |
| [PanzerarmeeAfrikaA4Booklet.pdf][] | [PanzerarmeeAfrikaLetterBooklet.pdf][]  |
| [PanzerarmeeAfrikaA3Booklet.pdf][] | [PanzerarmeeAfrikaTabloidBooklet.pdf][] |
| [materialsA4.pdf][]	             | [materialsLetter.pdf][]	               |
| [splitboardA4.pdf][]	             | [splitboardLetter.pdf][]	               |
| [splitboardA3.pdf][]	             | [splitboardTabloid.pdf][]	           |


Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## SPI and AHGC colours 

The material is also available in the classic SPI and AHGC colours.
The colours are not perfect matches, but as close as I could get them
with the material I had.  Any suggestions are welcome. 

### SPI 

| *A4 Series*                           | *Letter Series*                            |
| --------------------------------------|--------------------------------------------|
| [PanzerarmeeAfrikaA4SPI.pdf][]        | [PanzerarmeeAfrikaLetterSPI.pdf][]  	     |
| [PanzerarmeeAfrikaA4BookletSPI.pdf][] | [PanzerarmeeAfrikaLetterBookletSPI.pdf][]  |
| [PanzerarmeeAfrikaA3BookletSPI.pdf][] | [PanzerarmeeAfrikaTabloidBookletSPI.pdf][] |
| [materialsA4SPI.pdf][]	            | [materialsLetterSPI.pdf][]	             |
| [splitboardA4SPI.pdf][]	            | [splitboardLetterSPI.pdf][]	             |
| [splitboardA3SPI.pdf][]	            | [splitboardTabloidSPI.pdf][]	             |
| [hexesSPI.pdf][] ||

### AHGC

| *A4 Series*                            | *Letter Series*                             |
| ---------------------------------------|---------------------------------------------|
| [PanzerarmeeAfrikaA4AHGC.pdf][]        | [PanzerarmeeAfrikaLetterAHGC.pdf][]  	   |
| [PanzerarmeeAfrikaA4BookletAHGC.pdf][] | [PanzerarmeeAfrikaLetterBookletAHGC.pdf][]  |
| [PanzerarmeeAfrikaA3BookletAHGC.pdf][] | [PanzerarmeeAfrikaTabloidBookletAHGC.pdf][] |
| [materialsA4AHGC.pdf][]	             | [materialsLetterAHGC.pdf][]	               |
| [splitboardA4AHGC.pdf][]	             | [splitboardLetterAHGC.pdf][]	               |
| [splitboardA3AHGC.pdf][]	             | [splitboardTabloidAHGC.pdf][]	           |
| [hexesAHGC.pdf][] ||


## VASSAL modules 

Also available is a [VASSAL](https://vassalengine.org) module

- [VMOD][PanzerarmeeAfrika.vmod]
- [VMOD (SPI colours)][PanzerarmeeAfrikaSPI.vmod]
- [VMOD (AHGC colours)][PanzerarmeeAfrikaAHGC.vmod]

The module is generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  The modules
features

- Embedded rules
- Tutorial
- Extensive help on how to use the module
- Turn and phase track (essential for automation)
  - Operations only allowed in relevant phase
- Automation (can be disabled)
  - OOC2 determination
    - Includes both AHGC and SPI method, chosen by user
  - Battle markers
  - Calculate Combat Factor (CF) differentials, including effects of
    terrain and supply
  - Determine combat outcome
  - Counter attack CF differentials, and combat outcome
  - Overrun CF differentials, outcome, counter attack
  - Replacement points
  - Reinforcements
    - Including optional reinforcement options
    - Mutually exclusive reinforcement options inhibited
  - Malta invasion
  - "Victory points", including penalty from reinforcement options
  - Clearing of OOS and OOC2 states
- Charts
- OOBs
- Separate layers for markers, transport units, supply installations,
  units
- Transport units are `Mat`s that carry supply installations as
  `Cargo`
  - Supply installations cannot move otherwise
  - Special "Egyptian Railroad" marker to transport along rail.
- Automatic battle, moved, OOS, OOC2, etc. markers cleared
- Toggle movement trail button
- Inventory by status
- Symbolic dice
- Optional rules
  - Reaction movment - recalculate CF differentials
  - Italian infantry reliability - keep track of Italian infantry losses
    - Automatic surrender resolution

Note, it is not really feasible to enforce movement rules with VASSAL,
which means this module ''does not''
- Keep track of MFs used
- Trace supply lines (users must flag units or stacks of units
  themselves)
- Trace (Maximum) Attack Supply (users must flag units or stacks of
  units themselves)

## Previews

![Front](.imgs/front.png)
![Board](.imgs/hexes.png)
![Counters](.imgs/cm-chits.png)
![Counters](.imgs/al-chits.png)
![Counters](.imgs/ax-chits.png)
![Allied OOB](.imgs/al-oob.png)
![Axis OOB](.imgs/ax-oob.png)
![Charts](.imgs/tables.png)
![VASSAL module](.imgs/vassal.png)
![Components](.imgs/photo_components.png)
![Allied units on OOB](.imgs/photo_allied_oob.png)
![Board compared to AHGC board](.imgs/photo_ahgc_board.png)
![Counters compared to AHGC counters](.imgs/photo_ahgc_counters.png)

### SPI 

![Front](.imgs/front+SPI.png)
![Board](.imgs/hexes+SPI.png)
![Counters](.imgs/cm-chits+SPI.png)
![Counters](.imgs/al-chits+SPI.png)
![Counters](.imgs/ax-chits+SPI.png)
![Allied OOB](.imgs/al-oob+SPI.png)
![Axis OOB](.imgs/ax-oob+SPI.png)
![Charts](.imgs/tables+SPI.png)
![VASSAL module](.imgs/vassal+SPI.png)

### AHGC 

![Front](.imgs/front+AHGC.png)
![Board](.imgs/hexes+AHGC.png)
![Counters](.imgs/cm-chits+AHGC.png)
![Counters](.imgs/al-chits+AHGC.png)
![Counters](.imgs/ax-chits+AHGC.png)
![Allied OOB](.imgs/al-oob+AHGC.png)
![Axis OOB](.imgs/ax-oob+AHGC.png)
![Charts](.imgs/tables+AHGC.png)
![VASSAL module](.imgs/vassal+AHGC.png)

<!-- ![VASSAL detail](.imgs/vassal_detail.png)
![Components](.imgs/photo_components.png)
![Setup](.imgs/photo_setup.png) -->

## Differences between SPI and AHGC version 

The Print'n'Play documents, as well as the VASSAL document, supports
both versions of the rules: The SPI version from 1973 with errata from
1974, and the 1982 AHGC version with errata from 1985. 

A [concise red/blue difference][diff] document can be found at BoardGameGeek

The major differences between the two version are 

- AHGC allows faction to promote their victory level by holding on to
  Tobruk for the majority of the game, should the game run all 20
  turns.
  
  The point of this is to encourage the factions to be more aggressive
  and address concerns raised that the Axis faction could win a
  marginal victory by being very passive.
  
- Allied $\text{C}^2$ resolution was changed.  In the SPI version, a
  single die is rolled, and a table is consulted to see which hexes
  are out-of-$\text{C}^2$.  However, in that table, not all hexes are
  equally likely - specifically, hexes which end in 8 or 9 only has a
  1 in 6 risk of being out-of-$\text{C}^2$, while other hexes has a 1
  in 3 risk. 
  
  Some has attributed this to a profound design decision which
  supposedly encourages the Allied faction to set-up defences in
  specific areas of the map.  However, it seems much more likely that
  the original design wanted a single die roll and then came up with a
  somewhat botched table. 
  
  In the AHGC, the Allied faction rolls a die for every hex occupied
  by Allied units, and there's a 1 in 6, independent, risk of a hex
  being out-of-$\text{C}^2$.  While not an elegant solution to the
  problem, it removes an arbitrary bias present in the SPI version, at
  the cost of some extra die rolls. 
  
- The AHGC errata in [_The General_, **22**, #1,
  p46](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%201.pdf#page=46)
  stipulates that a supply installation that is Out-of-$\text{C}^2$
  _may not_ provide _Maximum Attack Supply_.  It is arguable whether
  this is a real difference to the SPI edition, as it seems to have
  been intent all along. 
  
Beside these to changes, the two version, when we take errata into
account, are identical. 

## Articles 
- "Consolidated Errata - PanzerArmee Afrika, Desert War, KampfPanzer,
  East is Red and Leipzig (as of April 1974)", [_Moves_, **14**,
  p21-23](https://strategyandtacticspress.com/library-files/Moves%20Issue14.pdf#page=21), 1974.
- SPI R&D staff, "Panzerarmee Afrika Brainstorming", [_Moves_, **17**,
  p13-16](https://strategyandtacticspress.com/library-files/Moves%20Issue17.pdf#page=13), 1974. 
- Vickers, R., "Command Control", [_Phoenix_, **3**,
  p9](https://www.spigames.net/Phoenix/Phoenix3.pdf#page=9), 1976.
- Nash, R., "How not to fight Rommel", [_Phoenix_, **10**,
  p9](https://www.spigames.net/Phoenix/Phoenix10.pdf#page=10), 1977.
- Spence, J., "Game Problem: Panzerarmee Afrika", [_Phoenix_, **13**,
  p15](https://www.spigames.net/Phoenix/Phoenix13.pdf#page=15), 1978.
- Spence, J., "Problem Result: Panzerarmee Afrika", [_Phoenix_,
    **14**,
    p12](https://www.spigames.net/Phoenix/Phoenix14.pdf#page=12), 1978.
- Medrow, B., "Over Burning Sands: A Tour Through Panzerarmee Afrika:
  Rommel in the Desert, April 1941 -- November 1942", [_The General_,
  **22**, #1, p5-8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%201.pdf#page=5), 1985.
- Martin, R.A., "To the Borderline: Opening Strategies for Panzerarmee
  Afrika: Rommel in the Desert, April 1941 -- November 1942", [_The
  General_, **22**, #1, p9-14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%201.pdf#page=9), 1985.
- "Contest \#125", [_The General_, **22**, #1,
  p14](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%201.pdf#page=14), 1985.
- "Question box: Panzerarmee Afrika", [_The General_, **22**, #1,
  p46](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%201.pdf#page=46), 1985.
- "Infiltrator Report", [_The General_, **22**, #2,
  p47](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2022%20No%202.pdf#page=47), 1985.
- Shaw, M., "With Rommel (and Monty) in the Desert --- A Study of Axis
  and Allied Play in Panzerarmee Afrika: Rommel in the Desert, April
  1941 -- November 1942", [_The General_, **25**, #6,
  p27-31](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2025%20No%206.pdf#page=27), 1989.
- [SPI originals](https://www.spigames.net/rules_downloads.htm#P)


## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This
package, combined with the power of LaTeX, produces high-quality
documents, with vector graphics to ensure that everything scales.

## License 

These materials a licensed under the [Creative Commons
Attribution-ShareAlike 4.0 Internationalo
License](http://creativecommons.org/licenses/by-sa/4.0/).

© 2024 Christian Holm Christensen

[spigames]: https://www.spigames.net/rules_downloads.htm#P
[simpubs]: https://nextcloud.simpubs.org/s/r7t2T5WZ26QFAjy
[diff]: https://boardgamegeek.com/thread/3439035

[artifacts.zip]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/download?job=dist

[PanzerarmeeAfrikaA4.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/PanzerarmeeAfrikaA4.pdf?job=dist
[PanzerarmeeAfrikaA4Booklet.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/PanzerarmeeAfrikaA4Booklet.pdf?job=dist
[PanzerarmeeAfrikaA3Booklet.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/PanzerarmeeAfrikaA3Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/materialsA4.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/splitboarlrgdA4.pdf?job=dist

[hexes.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/hexes.pdf?job=dist

[PanzerarmeeAfrika.vmod]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master/PanzerarmeeAfrika.vmod?job=dist


[PanzerarmeeAfrikaLetter.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master/PanzerarmeeAfrikaLetter.pdf?job=dist
[PanzerarmeeAfrikaLetterBooklet.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master/PanzerarmeeAfrikaLetterBooklet.pdf?job=dist
[PanzerarmeeAfrikaTabloidBooklet.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master/PanzerarmeeAfrikaTabloidBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master/materialsLetter.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master/splitboardLetter.pdf?job=dist

<!-- SPI themed -->

[PanzerarmeeAfrikaA4SPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/PanzerarmeeAfrikaA4.pdf?job=dist
[PanzerarmeeAfrikaA4BookletSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/PanzerarmeeAfrikaA4Booklet.pdf?job=dist
[PanzerarmeeAfrikaA3BookletSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/PanzerarmeeAfrikaA3Booklet.pdf?job=dist
[materialsA4SPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/materialsA4.pdf?job=dist
[splitboardA3SPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/splitboardA3.pdf?job=dist
[splitboardA4SPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/splitboarlrgdA4.pdf?job=dist

[hexesSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/hexes.pdf?job=dist

[PanzerarmeeAfrikaSPI.vmod]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+SPI/PanzerarmeeAfrika.vmod?job=dist


[PanzerarmeeAfrikaLetterSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+SPI/PanzerarmeeAfrikaLetter.pdf?job=dist
[PanzerarmeeAfrikaLetterBookletSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+SPI/PanzerarmeeAfrikaLetterBooklet.pdf?job=dist
[PanzerarmeeAfrikaTabloidBookletSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+SPI/PanzerarmeeAfrikaTabloidBooklet.pdf?job=dist
[materialsLetterSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+SPI/materialsLetter.pdf?job=dist
[splitboardTabloidSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+SPI/splitboardTabloid.pdf?job=dist
[splitboardLetterSPI.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+SPI/splitboardLetter.pdf?job=dist

<!-- AHGC themed -->

[PanzerarmeeAfrikaA4AHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/PanzerarmeeAfrikaA4.pdf?job=dist
[PanzerarmeeAfrikaA4BookletAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/PanzerarmeeAfrikaA4Booklet.pdf?job=dist
[PanzerarmeeAfrikaA3BookletAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/PanzerarmeeAfrikaA3Booklet.pdf?job=dist
[materialsA4AHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/materialsA4.pdf?job=dist
[splitboardA3AHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/splitboardA3.pdf?job=dist
[splitboardA4AHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/splitboarlrgdA4.pdf?job=dist

[hexesAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/hexes.pdf?job=dist

[PanzerarmeeAfrikaAHGC.vmod]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-A4-master+AHGC/PanzerarmeeAfrika.vmod?job=dist


[PanzerarmeeAfrikaLetterAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+AHGC/PanzerarmeeAfrikaLetter.pdf?job=dist
[PanzerarmeeAfrikaLetterBookletAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+AHGC/PanzerarmeeAfrikaLetterBooklet.pdf?job=dist
[PanzerarmeeAfrikaTabloidBookletAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+AHGC/PanzerarmeeAfrikaTabloidBooklet.pdf?job=dist
[materialsLetterAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+AHGC/materialsLetter.pdf?job=dist
[splitboardTabloidAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+AHGC/splitboardTabloid.pdf?job=dist
[splitboardLetterAHGC.pdf]: https://gitlab.com/wargames_tex/paa_tex/-/jobs/artifacts/master/file/PanzerarmeeAfrika-Letter-master+AHGC/splitboardLetter.pdf?job=dist




